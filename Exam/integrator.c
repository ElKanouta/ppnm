/* Based on integrator.c of adaptive integration exercise*/
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define M_PI 3.14159

//try
//Instead of the factors given in eq. 48  we'll use random numbers



double my_integrator(double f(double), double a, double b, double acc, double eps, double f2, double f3, double *err){
    
    double f1=f(a+((double)rand()/RAND_MAX)*(b-a));
    double f4=f(a+((double)rand()/RAND_MAX)*(b-a));
	double Q=(2*f1+f2+f3+2*f4)/6*(b-a); //higher order rule
    double q=(f1+f4+f2+f3)/4*(b-a); //lower order rule

    // // try random
    // double Q=(((double)rand()/RAND_MAX)*f1+((double)rand()/RAND_MAX)*f2+((double)rand()/RAND_MAX)*f3+((double)rand()/RAND_MAX)*f4)*(b-a); //higher order rule
    // double q=(((double)rand()/RAND_MAX)*f1+((double)rand()/RAND_MAX)*f4+((double)rand()/RAND_MAX)*f2+((double)rand()/RAND_MAX)*f3)*(b-a); //lower order rule

    double tol=acc+eps*fabs(Q); //tolerance
    *err=fabs(Q-q)/2; //error
    if (*err<tol){
        return Q;
    }
    else{//call the function again for two intervals
        double Q1=my_integrator(f,a,(a+b)/2,acc/sqrt(2.),eps,f1,f2,err);
        double Q2=my_integrator(f,(a+b)/2,b,acc/sqrt(2.),eps,f3,f4,err);
        return Q1+Q2;

    }


}

double adapt(double f(double), double a, double b, double acc, double eps, double *err){
    
    double f2,f3;
    // accept infinite limits(exercise C)
    if (isinf(a) && isinf(b)) {
		double g(double t) { //variable transformation, equation 58
			return (f((1-t)/t)+f(-(1-t)/t))/(t*t); 
		}
		return my_integrator(g,0.0,1.0,acc,eps,f2,f3,err);
    }
    if(isinf(a)){
        double g(double t){ //variable transformation equation 62
            return f(b-(1-t)/t)/(t*t);
        }
        return my_integrator(g,0.0,1.0,acc,eps,f2,f3,err);
    }

    if(isinf(b)){
        double g(double t){//variable transformation equation 60
            return f(a+(1-t)/t)/(t*t);
        }
        return my_integrator(g,0.0,1.0,acc,eps,f2,f3,err);
    }
    
    //calculate f2, f3
    f2=f(a+((double)rand()/RAND_MAX)*(b-a));
    f3=f(a+((double)rand()/RAND_MAX)*(b-a));

   
    return my_integrator(f,a,b,acc,eps,f2,f3,err);
    

}

// double clenshaw_curtis(double f(double),double a,double b,double acc,double eps,double *err){
// 	double g(double t){ //variable transformation
//         return f( (a+b)/2+(a-b)/2*cos(t) )*sin(t)*(b-a)/2;
//     }
// 	return adapt(g,0.0, M_PI,acc,eps,err);
// }