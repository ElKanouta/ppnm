#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_blas.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void backsub(gsl_matrix *R, gsl_vector *x);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);

void matrix_print(FILE* stream,const char* message, gsl_matrix* M){
	fprintf(stream,"%s\n",message);
	for(int i=0;i<M->size1;i++){
		for(int j=0;j<M->size2;j++){
			fprintf(stream,"%8.3g ",gsl_matrix_get(M,i,j));
		}
		fprintf(stream,"\n");
	}
}

int main(){
//check that qr_gs_decomp works
   
    size_t n=4;
    size_t m=2;
    gsl_matrix* A=gsl_matrix_alloc(n,m);
    gsl_matrix* Q=gsl_matrix_alloc(n,m);
    gsl_matrix* R=gsl_matrix_alloc(m,m);
    //initiallize A
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            double r=rand()/(double)RAND_MAX;
            gsl_matrix_set(A,i,j,r);
            gsl_matrix_set(Q,i,j,r);
        }
    }
//factoriize A into QR
    qr_gs_decomp(Q,R);
    printf("Exercise A\n");
    matrix_print(stdout,"A=",A);
    matrix_print(stdout,"Q=",Q);
    matrix_print(stdout,"R=",R);

//check that QtransQ=1
    gsl_matrix *QTQ=gsl_matrix_alloc(m,m);
    
    gsl_blas_dsyrk(CblasUpper,CblasTrans,1,Q,0,QTQ);
    matrix_print(stdout,"QT Q",QTQ);


//check QR=A
    gsl_matrix *QR=gsl_matrix_alloc(n,m);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,Q,R,0,QR);
    matrix_print(stdout,"QR=",QR);



//***********************************************************//
//check that qr_gs_solve works
    int k=3;
    gsl_vector *b=gsl_vector_alloc(k);
    gsl_vector *x=gsl_vector_alloc(k);
    gsl_vector *btest=gsl_vector_alloc(k);
    gsl_matrix *A_2=gsl_matrix_alloc(k,k);
    gsl_matrix *R_2=gsl_matrix_alloc(k,k);
    gsl_matrix *Q_2=gsl_matrix_alloc(k,k);
    fprintf(stdout,"\n\nSolve\n");
    //randon initial square matrix
    for(int i=0;i<k;i++){
        for(int j=0;j<k;j++){
            double r_2=rand()/(double)RAND_MAX;
            gsl_matrix_set(A_2,i,j,r_2);
            gsl_matrix_set(Q_2,i,j,r_2);
            
        }
    }
    matrix_print(stdout,"Random matrix A=",Q_2);

    //random vector
    for(int i=0;i<k;i++){
        gsl_vector_set(b,i,(double)rand()/RAND_MAX);
    }
    fprintf(stdout,"Random vector b:\n");
    gsl_vector_fprintf(stdout,b," %g");
    //factorize into QR
    qr_gs_decomp(Q_2,R_2);
    fprintf(stdout,"factorize");
    matrix_print(stdout,"Q=",Q_2);
    matrix_print(stdout,"R=",R_2);
    

    //solve QRb=x
    qr_gs_solve(Q_2,R_2,b,x);
    printf("x\n");
    gsl_vector_fprintf(stdout,x,"%g");


//check Ax=b 
    gsl_blas_dgemv(CblasNoTrans,1.0,A_2,x,0.0,btest);
    printf("\nSOLVE QRx=b\n");
    printf("vector b:\n");
    gsl_vector_fprintf(stdout,b,"%g");
    printf("\ncheck Ax=b\n");
    gsl_vector_fprintf(stdout,btest,"%g");
    printf("\n\nExercise B\n");
//****************************************************************
//EXERCISE B //to do
    gsl_matrix *B=gsl_matrix_calloc(k,k);
    gsl_matrix *AB=gsl_matrix_alloc(k,k);
    
    qr_gs_inverse(Q_2,R_2,B);
    
    matrix_print(stdout,"A inverse:",B);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_2,B,0,AB);
    matrix_print(stdout,"Check: AB=",AB);

    gsl_matrix_free(A);
    gsl_matrix_free(Q);
    gsl_matrix_free(R);
    gsl_matrix_free(QTQ);
    gsl_matrix_free(QR);
    gsl_matrix_free(A_2);
    gsl_matrix_free(R_2);
    gsl_matrix_free(Q_2);
    gsl_vector_free(b);
    gsl_vector_free(x);
    gsl_vector_free(btest);
    gsl_matrix_free(B);
    gsl_matrix_free(AB);
    
    return 0;
}
