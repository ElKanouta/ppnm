#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdlib.h>
#include<math.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
    int n,m;//size
    A=gsl_matrix_alloc(n,m);
    gsl_matrix* Q=gsl_matrix_alloc(n,m);
    R=gsl_matrix_alloc(m,m);
    
    double inner;
    int i,j;
    for(i=0;i<m;i++){//for each collumn-vector
        for(j=0;j<n;j++){//vector elements
            inner+=gsl_matrix_get(Q,j,i)*gsl_matrix_get(Q,j,i);
            //changed A to Q
        }
        gsl_matrix_set(R,i,i,sqrt(inner));
        
        for(int l=0;l<n;l++){
            gsl_matrix_set(Q,l,i,gsl_matrix_get(Q,l,i)/gsl_matrix_get(R,i,i));

        }
        for(j=i+1;j<m;j++){
            for(int l=0;l<n;l++){
                gsl_matrix_set(R,i,j,gsl_matrix_get(Q,l,j)*gsl_matrix_get(Q,l,j));
                gsl_matrix_set(R,j,l,0);
            }
            
            
            for(int l=0;l<n;l++){
                gsl_matrix_set(Q,l,j,gsl_matrix_get(Q,l,j)-gsl_matrix_get(Q,l,i)*gsl_matrix_get(R,l,j));
            }
            
        }
    }
    
    gsl_matrix_free(A);
    gsl_matrix_free(Q);
    gsl_matrix_free(R);
}

int main(){
    //check that qr_gs_decomp works
    int n=2, m=3;
    gsl_matrix* A=gsl_matrix_alloc(n,m);
    gsl_matrix* Q=gsl_matrix_alloc(n,m);
    gsl_matrix* R=gsl_matrix_alloc(m,m);
    //initiallize A
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            double r=rand()/(double)RAND_MAX;
            gsl_matrix_set(A,i,j,r);
            gsl_matrix_set(Q,i,j,r);
        }
    }
    printf("matrix A:\n");
    gsl_matrix_fprintf(stdout,A,"%g");
    
    printf("matrix Q:\n");
    gsl_matrix_fprintf(stdout,Q,"%g");
    qr_gs_decomp(A,R);
    printf("matrix R:\n");
    gsl_matrix_fprintf(stdout,R,"%g");

    gsl_matrix_free(A);
    gsl_matrix_free(Q);
    gsl_matrix_free(R);
    return 0;
}