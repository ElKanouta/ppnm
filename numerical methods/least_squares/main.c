#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void leastsq(int m, double f(int i, double x),gsl_vector* x,gsl_vector* y, gsl_vector* dy,gsl_vector* c,gsl_matrix* S);

void matrix_print(FILE* stream,const char* message, gsl_matrix* M){
	fprintf(stream,"%s\n",message);
	for(int i=0;i<M->size1;i++){
		for(int j=0;j<M->size2;j++){
			fprintf(stream,"%8.3g ",gsl_matrix_get(M,i,j));
		}
		fprintf(stream,"\n");
	}
}

double fitting_funcs(int i, double x){
   switch(i){
   case 0: return log(x); break;
   case 1: return 1.0;   break;
   case 2: return x;     break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}


int main(){
    //data
    double x_data[]={0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9};
    double y_data[]={-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
    double dy_data[]={1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};

    int n=sizeof(x_data)/sizeof(x_data[0]);
    
    //for data plot
    for(int i=0;i<n;i++){
        fprintf(stderr,"%g %g %g\n",x_data[i],y_data[i],dy_data[i]);
    }
    fprintf(stderr,"\n\n\n");
    gsl_vector* x=gsl_vector_alloc(n);
    gsl_vector* y=gsl_vector_alloc(n);
    gsl_vector* dy=gsl_vector_alloc(n);

    for(int i=0;i<n;i++){
        gsl_vector_set(x,i,x_data[i]);
        gsl_vector_set(y,i,y_data[i]);
        gsl_vector_set(dy,i,dy_data[i]);
    }
    int m=3;
    gsl_vector* c=gsl_vector_alloc(m);
    gsl_matrix* S=gsl_matrix_alloc(m,m);
    leastsq(m,fitting_funcs,x,y,dy,c,S);
    gsl_vector_fprintf(stderr,c,"Coefficients: %g\t");


//fit the data
    double fit(double x){
        double s=0;
        for(int j=0;j<m;j++){
            s+=gsl_vector_get(c,j)*fitting_funcs(j,x);
        }
        return s;
    }
    fprintf(stderr,"\n");
    matrix_print(stderr,"Covariance matrix=",S);
    fprintf(stderr,"Coefficients with errors:\n");
    double sigma_c=0;
    for(int i=0;i<3;i++){
        sigma_c = sqrt(gsl_matrix_get(S,i,i));
    
        fprintf(stderr,"%g +/- %g\n",gsl_vector_get(c,i),sigma_c);
    }
    fprintf(stderr,"\n\n");
    
    double fit_error(double x){
        double sum=0;
        for(int i=0;i<m;i++){
            for(int j=0;j<m;j++){
                sum+=fitting_funcs(i,x)*gsl_matrix_get(S,i,j)*fitting_funcs(j,x);
            }
        }
        return sqrt(sum);
    }
    
    // fit for variable z between the data points, with small step
    
    for(double z=x_data[0];z<=x_data[8];z+=0.1){
            
        fprintf(stderr, "%g %g %g %g\n",z,fit(z),fit(z)+fit_error(z),fit(z)-fit_error(z));
        
    }
    fprintf(stderr,"\n\n");
    
    
    gsl_vector_free(x);
    gsl_vector_free(y);
    gsl_vector_free(dy);
    gsl_vector_free(c);
    gsl_matrix_free(S);

    return 0;
}

