#include<stdio.h>
#include<stdlib.h>

#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);

void leastsq(int m, double f(int i, double x),gsl_vector* x,gsl_vector* y, gsl_vector* dy,gsl_vector* c,gsl_matrix* S){
//size2, fitting functions, data x,y,dy, coef. c, covariant matrix
   int n=x->size;
   gsl_matrix* A=gsl_matrix_alloc(n,m);
   gsl_matrix* Q=gsl_matrix_alloc(n,m);
   gsl_matrix* R=gsl_matrix_alloc(m,m);
   gsl_vector* b=gsl_vector_alloc(n);

   //input data
   for(int i=0;i<n;i++){
      double xi=gsl_vector_get(x,i);
      double yi=gsl_vector_get(y,i);
      double dyi=gsl_vector_get(dy,i);
      gsl_vector_set(b,i,yi/dyi);
      for(int j=0;j<m;j++){
         gsl_matrix_set(A,i,j,f(j,xi)/dyi);
      }
   }
   qr_gs_decomp(A,R);
   qr_gs_solve(A,R,b,c);

   //covariance matrix (ATA)^-1=(RTR)^-1
   gsl_matrix* R_inv=gsl_matrix_alloc(m,m);
   
   //use qr inverse to find R^-1
   gsl_matrix *I=gsl_matrix_alloc(m,m);
   gsl_matrix_set_identity(I);
   

   qr_gs_inverse(I,R,R_inv);
   gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,R_inv,R_inv,0,S);
   gsl_matrix_free(A);
   gsl_matrix_free(Q);
   gsl_matrix_free(R);
   gsl_vector_free(b);
   gsl_matrix_free(R_inv);
   gsl_matrix_free(I);
   


   

}