#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>

void matrix_print(FILE* stream,const char* message, gsl_matrix* M);
	
//eliminate off- diagonal elements in first row-->min eigenvalue
double first_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
    int n=A->size1;
    int sweeps=0;
    int changed; //check if converged
    for(int i=0;i<n;i++){
        gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
    }
    gsl_matrix_set_identity(V);
    
   do{
        changed=0;
        sweeps++;
        int p,q;
        for(p=0;p<1;p++){
            for(q=p+1;q<n;q++){
                double App=gsl_vector_get(e,p);
                double Aqq=gsl_vector_get(e,q);
                double Apq=gsl_matrix_get(A,p,q);
                //angle chosen to put Apq'=0
                double phi=0.5*atan2(2*Apq,Aqq-App);
                double c=cos(phi);
                double s=sin(phi);
                double App_prime=c*c*App-2*s*c*Apq+s*s*Aqq;
                double Aqq_prime=s*s*App+2*s*c*Apq+c*c*Aqq;
                //check if the matrix elements have changed
                if(App_prime!=App || Aqq_prime!=Aqq){
                    changed=1;
                    gsl_vector_set(e,p,App_prime);
                    gsl_vector_set(e,q,Aqq_prime);
                    gsl_matrix_set(A,p,q,0.0);
                    for(int i=0;i<p;i++){
                        double Aip=gsl_matrix_get(A,i,p);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
                        gsl_matrix_set(A,i,q,s*Aip+c*Aiq);
                    
                    }
                    // printf("for i<p\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=p+1;i<q;i++){//check order of i,p,  i,q
                        double Api=gsl_matrix_get(A,p,i);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,p,i,c*Api-s*Aiq);
                        gsl_matrix_set(A,i,q,c*Aiq+s*Api);

                    }
                    // printf("for i<q\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=q+1;i<n;i++){
                        double Api=gsl_matrix_get(A,p,i);
                        double Aqi=gsl_matrix_get(A,q,i);
                        gsl_matrix_set(A,p,i,c*Api-s*Aqi);
				        gsl_matrix_set(A,q,i,c*Aqi+s*Api); 
                    }
                    // printf("for i<n\n");
                    // matrix_print(stdout,"A=",A);
			        for(int i=0;i<n;i++){
				        double Vip=gsl_matrix_get(V,i,p);
				        double Viq=gsl_matrix_get(V,i,q);
				        gsl_matrix_set(V,i,p,c*Vip-s*Viq);
				        gsl_matrix_set(V,i,q,c*Viq+s*Vip);
                    }
                    
			    } 
            }
            
        }
    }
    while(changed!=0);
return sweeps;
}
double second_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
    int n=A->size1;
    int sweeps=0;
    int changed; //check if converged
    for(int i=0;i<n;i++){
        gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
    }
    gsl_matrix_set_identity(V);
    do{
        changed=0;
        sweeps++;
        int p,q;
        for(p=0;p<2;p++){
            for(q=p+1;q<n;q++){
                double App=gsl_vector_get(e,p);
                double Aqq=gsl_vector_get(e,q);
                double Apq=gsl_matrix_get(A,p,q);
                //angle chosen to put Apq'=0
                double phi=0.5*atan2(2*Apq,Aqq-App);
                double c=cos(phi);
                double s=sin(phi);
                double App_prime=c*c*App-2*s*c*Apq+s*s*Aqq;
                double Aqq_prime=s*s*App+2*s*c*Apq+c*c*Aqq;
                //check if the matrix elements have changed
                if(App_prime!=App || Aqq_prime!=Aqq){
                    changed=1;
                    gsl_vector_set(e,p,App_prime);
                    gsl_vector_set(e,q,Aqq_prime);
                    gsl_matrix_set(A,p,q,0.0);
                    for(int i=0;i<p;i++){
                        double Aip=gsl_matrix_get(A,i,p);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
                        gsl_matrix_set(A,i,q,s*Aip+c*Aiq);
                    
                    }
                    // printf("for i<p\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=p+1;i<q;i++){//check order of i,p,  i,q
                        double Api=gsl_matrix_get(A,p,i);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,p,i,c*Api-s*Aiq);
                        gsl_matrix_set(A,i,q,c*Aiq+s*Api);

                    }
                    // printf("for i<q\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=q+1;i<n;i++){
                        double Api=gsl_matrix_get(A,p,i);
                        double Aqi=gsl_matrix_get(A,q,i);
                        gsl_matrix_set(A,p,i,c*Api-s*Aqi);
				        gsl_matrix_set(A,q,i,c*Aqi+s*Api); 
                    }
                    // printf("for i<n\n");
                    // matrix_print(stdout,"A=",A);
			        for(int i=0;i<n;i++){
				        double Vip=gsl_matrix_get(V,i,p);
				        double Viq=gsl_matrix_get(V,i,q);
				        gsl_matrix_set(V,i,p,c*Vip-s*Viq);
				        gsl_matrix_set(V,i,q,c*Viq+s*Vip);
                    }
                    
			    } 
            }
            
        }
    }
    while(changed!=0);
return sweeps;
}

//change definition og angles to find max eigenvalue
double max_eigen(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
    int n=A->size1;
    int sweeps=0;
    int changed; //check if converged
    for(int i=0;i<n;i++){
        gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
    }
    gsl_matrix_set_identity(V);
    do{
        changed=0;
        sweeps++;
        int p,q;
        for(p=0;p<1;p++){
            for(q=p+1;q<n;q++){
                double App=gsl_vector_get(e,p);
                double Aqq=gsl_vector_get(e,q);
                double Apq=gsl_matrix_get(A,p,q);
                //angle chosen to put Apq'=0
                double phi=0.5*atan2(-2*Apq,-Aqq+App); //change signs in phi
                double c=cos(phi);
                double s=sin(phi);
                double App_prime=c*c*App-2*s*c*Apq+s*s*Aqq;
                double Aqq_prime=s*s*App+2*s*c*Apq+c*c*Aqq;
                //check if the matrix elements have changed
                if(App_prime!=App || Aqq_prime!=Aqq){
                    changed=1;
                    gsl_vector_set(e,p,App_prime);
                    gsl_vector_set(e,q,Aqq_prime);
                    gsl_matrix_set(A,p,q,0.0);
                    for(int i=0;i<p;i++){
                        double Aip=gsl_matrix_get(A,i,p);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
                        gsl_matrix_set(A,i,q,s*Aip+c*Aiq);
                    
                    }
                    // printf("for i<p\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=p+1;i<q;i++){//check order of i,p,  i,q
                        double Api=gsl_matrix_get(A,p,i);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,p,i,c*Api-s*Aiq);
                        gsl_matrix_set(A,i,q,c*Aiq+s*Api);

                    }
                    // printf("for i<q\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=q+1;i<n;i++){
                        double Api=gsl_matrix_get(A,p,i);
                        double Aqi=gsl_matrix_get(A,q,i);
                        gsl_matrix_set(A,p,i,c*Api-s*Aqi);
				        gsl_matrix_set(A,q,i,c*Aqi+s*Api); 
                    }
                    // printf("for i<n\n");
                    // matrix_print(stdout,"A=",A);
			        for(int i=0;i<n;i++){
				        double Vip=gsl_matrix_get(V,i,p);
				        double Viq=gsl_matrix_get(V,i,q);
				        gsl_matrix_set(V,i,p,c*Vip-s*Viq);
				        gsl_matrix_set(V,i,q,c*Viq+s*Vip);
                    }
                    
			    } 
            }
            
        }
    }
    while(changed!=0);
return sweeps;
}