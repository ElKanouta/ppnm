#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>

//int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
int first_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
double second_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
//double max_eigen(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

double third_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
    int n=A->size1;
    int sweeps=0;
    int changed; //check if converged
    for(int i=0;i<n;i++){
        gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
    }
    gsl_matrix_set_identity(V);
    
   do{
        changed=0;
        sweeps++;
        int p,q;
        for(p=0;p<3;p++){
            for(q=p+1;q<n;q++){
                double App=gsl_vector_get(e,p);
                double Aqq=gsl_vector_get(e,q);
                double Apq=gsl_matrix_get(A,p,q);
                //angle chosen to put Apq'=0
                double phi=0.5*atan2(2*Apq,Aqq-App);
                double c=cos(phi);
                double s=sin(phi);
                double App_prime=c*c*App-2*s*c*Apq+s*s*Aqq;
                double Aqq_prime=s*s*App+2*s*c*Apq+c*c*Aqq;
                //check if the matrix elements have changed
                if(App_prime!=App || Aqq_prime!=Aqq){
                    changed=1;
                    gsl_vector_set(e,p,App_prime);
                    gsl_vector_set(e,q,Aqq_prime);
                    gsl_matrix_set(A,p,q,0.0);
                    for(int i=0;i<p;i++){
                        double Aip=gsl_matrix_get(A,i,p);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
                        gsl_matrix_set(A,i,q,s*Aip+c*Aiq);
                    
                    }
                    // printf("for i<p\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=p+1;i<q;i++){//check order of i,p,  i,q
                        double Api=gsl_matrix_get(A,p,i);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,p,i,c*Api-s*Aiq);
                        gsl_matrix_set(A,i,q,c*Aiq+s*Api);

                    }
                    // printf("for i<q\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=q+1;i<n;i++){
                        double Api=gsl_matrix_get(A,p,i);
                        double Aqi=gsl_matrix_get(A,q,i);
                        gsl_matrix_set(A,p,i,c*Api-s*Aqi);
				        gsl_matrix_set(A,q,i,c*Aqi+s*Api); 
                    }
                    // printf("for i<n\n");
                    // matrix_print(stdout,"A=",A);
			        for(int i=0;i<n;i++){
				        double Vip=gsl_matrix_get(V,i,p);
				        double Viq=gsl_matrix_get(V,i,q);
				        gsl_matrix_set(V,i,p,c*Vip-s*Viq);
				        gsl_matrix_set(V,i,q,c*Viq+s*Vip);
                    }
                    
			    } 
            }
            
        }
    }
    while(changed!=0);
return sweeps;
}
double forth_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
    int n=A->size1;
    int sweeps=0;
    int changed; //check if converged
    for(int i=0;i<n;i++){
        gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
    }
    gsl_matrix_set_identity(V);
    
   do{
        changed=0;
        sweeps++;
        int p,q;
        for(p=0;p<4;p++){
            for(q=p+1;q<n;q++){
                double App=gsl_vector_get(e,p);
                double Aqq=gsl_vector_get(e,q);
                double Apq=gsl_matrix_get(A,p,q);
                //angle chosen to put Apq'=0
                double phi=0.5*atan2(2*Apq,Aqq-App);
                double c=cos(phi);
                double s=sin(phi);
                double App_prime=c*c*App-2*s*c*Apq+s*s*Aqq;
                double Aqq_prime=s*s*App+2*s*c*Apq+c*c*Aqq;
                //check if the matrix elements have changed
                if(App_prime!=App || Aqq_prime!=Aqq){
                    changed=1;
                    gsl_vector_set(e,p,App_prime);
                    gsl_vector_set(e,q,Aqq_prime);
                    gsl_matrix_set(A,p,q,0.0);
                    for(int i=0;i<p;i++){
                        double Aip=gsl_matrix_get(A,i,p);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
                        gsl_matrix_set(A,i,q,s*Aip+c*Aiq);
                    
                    }
                    // printf("for i<p\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=p+1;i<q;i++){//check order of i,p,  i,q
                        double Api=gsl_matrix_get(A,p,i);
                        double Aiq=gsl_matrix_get(A,i,q);
                        gsl_matrix_set(A,p,i,c*Api-s*Aiq);
                        gsl_matrix_set(A,i,q,c*Aiq+s*Api);

                    }
                    // printf("for i<q\n");
                    // matrix_print(stdout,"A=",A);
                    for(int i=q+1;i<n;i++){
                        double Api=gsl_matrix_get(A,p,i);
                        double Aqi=gsl_matrix_get(A,q,i);
                        gsl_matrix_set(A,p,i,c*Api-s*Aqi);
				        gsl_matrix_set(A,q,i,c*Aqi+s*Api); 
                    }
                    // printf("for i<n\n");
                    // matrix_print(stdout,"A=",A);
			        for(int i=0;i<n;i++){
				        double Vip=gsl_matrix_get(V,i,p);
				        double Viq=gsl_matrix_get(V,i,q);
				        gsl_matrix_set(V,i,p,c*Vip-s*Viq);
				        gsl_matrix_set(V,i,q,c*Viq+s*Vip);
                    }
                    
			    } 
            }
            
        }
    }
    while(changed!=0);
return sweeps;
}

void matrix_print(FILE* stream,const char* message, gsl_matrix* M){
	fprintf(stream,"%s\n",message);
	for(int i=0;i<M->size1;i++){
		for(int j=0;j<M->size2;j++){
			fprintf(stream,"%8.3g ",gsl_matrix_get(M,i,j));
		}
		fprintf(stream,"\n");
	}
}


int main(int argc, char** argv){
    int n=(argc>1? atoi(argv[1]):4);
    
    //size_t n=4;
    printf("\n\n***************** EXERCISE B *****************\n");
    gsl_matrix* A=gsl_matrix_alloc(n,n);
    gsl_matrix* B=gsl_matrix_alloc(n,n);
    gsl_matrix* BB=gsl_matrix_alloc(n,n);
    gsl_matrix* C=gsl_matrix_alloc(n,n);
    gsl_matrix* E=gsl_matrix_alloc(n,n);
    gsl_matrix* F=gsl_matrix_alloc(n,n);
    gsl_matrix* G=gsl_matrix_alloc(n,n);



    //initialize symmetric matrix A
    for(int i=0;i<n;i++){
        for(int j=i;j<n;j++){
            double random=(double)rand()/RAND_MAX;
            gsl_matrix_set(A,i,j,random);
            gsl_matrix_set(A,j,i,random);
        }
    }
    //copy initial matrix to B and C
    gsl_matrix_memcpy(B,A);
    gsl_matrix_memcpy(C,A);
    gsl_matrix_memcpy(E,A);
    gsl_matrix_memcpy(BB,A);
    gsl_matrix_memcpy(F,A);
    gsl_matrix_memcpy(G,A);



    gsl_matrix* V=gsl_matrix_alloc(n,n);
    gsl_vector* e=gsl_vector_alloc(n);
    gsl_vector* eigen=gsl_vector_alloc(n);

    //int sweeps=jacobi(A,e,V);
    
    //printf("sweeps=%d\n",sweeps);

    //print matrices
    printf("For the random summetric matrix of exercise A:\n");
    matrix_print(stdout,"A=",B);
    

    


//exercise B
    
    first_row(B,eigen,V);
    printf("\nMin. eigenvalue: %g\n",gsl_vector_get(eigen,0));
    
    second_row(E,eigen,V);
    printf("\nSecond lowest eigenvalue: %g\n",gsl_vector_get(eigen,1));
    
    third_row(F,eigen,V);
    printf("\nThird lowest eigenvalue: %g\n",gsl_vector_get(eigen,2));
    forth_row(G,eigen,V);
    printf("\nForth lowest eigenvalue: %g\n",gsl_vector_get(eigen,3));
    // printf("From definition, the algorithm gives the minimum eigenvalue.\nIn order to find the maximum value we change the definition of the angle phi\nfrom 0.5*atan2(2*Apq,Aqq-App) to 0.5*atan2(-2*Apq,-Aqq+App).\n");
    // max_eigen(BB,eigen,V);
    // printf("Max. eigenvalue: %g\n",gsl_vector_get(eigen,0));



    gsl_matrix_free(A);
    gsl_matrix_free(B);
    gsl_matrix_free(C);
    gsl_matrix_free(E);
    gsl_matrix_free(BB);
    gsl_matrix_free(F);
    gsl_matrix_free(G);



    gsl_matrix_free(V);
    gsl_vector_free(e);
    gsl_vector_free(eigen);

    // gsl_matrix_free(AV);
    // gsl_matrix_free(D);
    // gsl_matrix_free(D_test);
    return 0;

}