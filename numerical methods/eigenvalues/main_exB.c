#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>

//int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
int first_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
double second_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
double max_eigen(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

void matrix_print(FILE* stream,const char* message, gsl_matrix* M){
	fprintf(stream,"%s\n",message);
	for(int i=0;i<M->size1;i++){
		for(int j=0;j<M->size2;j++){
			fprintf(stream,"%8.3g ",gsl_matrix_get(M,i,j));
		}
		fprintf(stream,"\n");
	}
}


int main(int argc, char** argv){
    int n=(argc>1? atoi(argv[1]):4);
    
    //size_t n=4;
    printf("\n\n***************** EXERCISE B *****************\n");
    gsl_matrix* A=gsl_matrix_alloc(n,n);
    gsl_matrix* B=gsl_matrix_alloc(n,n);
    gsl_matrix* BB=gsl_matrix_alloc(n,n);
    gsl_matrix* C=gsl_matrix_alloc(n,n);
    gsl_matrix* E=gsl_matrix_alloc(n,n);


    //initialize symmetric matrix A
    for(int i=0;i<n;i++){
        for(int j=i;j<n;j++){
            double random=(double)rand()/RAND_MAX;
            gsl_matrix_set(A,i,j,random);
            gsl_matrix_set(A,j,i,random);
        }
    }
    //copy initial matrix to B and C
    gsl_matrix_memcpy(B,A);
    gsl_matrix_memcpy(C,A);
    gsl_matrix_memcpy(E,A);
    gsl_matrix_memcpy(BB,A);


    gsl_matrix* V=gsl_matrix_alloc(n,n);
    gsl_vector* e=gsl_vector_alloc(n);
    gsl_vector* eigen=gsl_vector_alloc(n);

    //int sweeps=jacobi(A,e,V);
    
    //printf("sweeps=%d\n",sweeps);

    //print matrices
    printf("For the random summetric matrix of exercise A:\n");
    matrix_print(stdout,"A=",B);
    

    


//exercise B
    
    first_row(B,eigen,V);
    printf("\nMin. eigenvalue: %g\n",gsl_vector_get(eigen,0));
    
    // second_row(E,eigen,V);
    // printf("Second lowest eigenvalue: %g\n",gsl_vector_get(eigen,1));
    
    printf("From definition, the algorithm gives the minimum eigenvalue.\nIn order to find the maximum value we change the definition of the angle phi\nfrom 0.5*atan2(2*Apq,Aqq-App) to 0.5*atan2(-2*Apq,-Aqq+App).\n");
    printf("Max. eigenvalue found using function max_eigen\nThe function is left as a comment so it's not taken into account in time evaluation for the min. eigenvalue.\n\n");
    // max_eigen(BB,eigen,V);
    // printf("Max. eigenvalue: %g\n",gsl_vector_get(eigen,0));



    gsl_matrix_free(A);
    gsl_matrix_free(B);
    gsl_matrix_free(C);
    gsl_matrix_free(E);
    gsl_matrix_free(BB);


    gsl_matrix_free(V);
    gsl_vector_free(e);
    gsl_vector_free(eigen);

    // gsl_matrix_free(AV);
    // gsl_matrix_free(D);
    // gsl_matrix_free(D_test);
    return 0;

}