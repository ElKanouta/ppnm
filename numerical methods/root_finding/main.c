#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>


void newton_with_jacobian(void f(gsl_vector* x, gsl_vector* fx,gsl_matrix* J),gsl_vector* x,double epsilon);
void newton(void f(gsl_vector* x, gsl_vector* fx),gsl_vector* x, double dx,double epsilon);
int gsl_multiroot(int f(gsl_vector* x, void * params, gsl_vector* fx),gsl_vector* x, double epsilon);

int calls_sys_J=0;
int calls_ros_J=0;
int calls_him_J=0;

int calls_sys=0;
int calls_ros=0;
int calls_him=0;


void sys_with_J(gsl_vector* xi, gsl_vector* fx,gsl_matrix* J){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    double A=10000;
    gsl_vector_set(fx,0,A*x*y-1);
    gsl_vector_set(fx,1,exp(-x)+exp(-y)-1-1.0/A);
    gsl_matrix_set(J,0,0,A*y);
    gsl_matrix_set(J,0,1,A*x);
    gsl_matrix_set(J,1,0,-exp(-x));
    gsl_matrix_set(J,1,1,-exp(-y));

    calls_sys_J++;
}

void rosenbrock_with_J(gsl_vector* xi, gsl_vector* fx,gsl_matrix* J){ //roots of gradient
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(fx,0,2*(1-x)*(-1)-100*2*2*x*(y-x*x)); //derivative over x
    gsl_vector_set(fx,1,100*2*(y-x*x)); //derivative over y
    gsl_matrix_set(J,0,0,2-100*4*(y-x*x)+100*8*x*x);
    gsl_matrix_set(J,0,1,-4*100*x);
    gsl_matrix_set(J,1,0,-4*100*x);
    gsl_matrix_set(J,1,1,2*100);
    calls_ros_J++;
}

void himmelblau_with_J(gsl_vector* xi,gsl_vector* fx,gsl_matrix* J){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(fx,0,2*2*x*(x*x+y-11)+2*(x+y*y-7)); //derivative over x
    gsl_vector_set(fx,1,2*(x*x+y-11)+2*2*y*(x+y*y-7)); //derivative over y
    gsl_matrix_set(J,0,0,4*(x*x+y-11)+4*x*2*x+2);
    gsl_matrix_set(J,0,1,4*x+2*2*y);
    gsl_matrix_set(J,1,0,2*2*x+2*2*y);
    gsl_matrix_set(J,1,1,2+4*2*y);
    calls_him_J++;
}

void sys(gsl_vector* xi, gsl_vector* fx){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    double A=10000;
    gsl_vector_set(fx,0,A*x*y-1);
    gsl_vector_set(fx,1,exp(-x)+exp(-y)-1-1.0/A);
    
    calls_sys++;
}

void rosenbrock(gsl_vector* xi, gsl_vector* fx){ //roots of gradient
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(fx,0,2*(1-x)*(-1)-100*2*2*x*(y-x*x)); //derivative over x
    gsl_vector_set(fx,1,100*2*(y-x*x)); //derivative over y
    
    calls_ros++;
}

void himmelblau(gsl_vector* xi,gsl_vector* fx){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(fx,0,2*2*x*(x*x+y-11)+2*(x+y*y-7)); //derivative over x
    gsl_vector_set(fx,1,2*(x*x+y-11)+2*2*y*(x+y*y-7)); //derivative over y
    
    calls_him++;
}

int rosenbrock_for_gsl(const gsl_vector * x, void * params, gsl_vector * f){
    const double x1=gsl_vector_get(x,0);
    const double x2=gsl_vector_get(x,1);
    double gr1=400*x1*x1*x1 -400*x1*x2 +2*x1 -2;
    double gr2=200*x2 -200*x1*x1;
    gsl_vector_set(f,0,gr1);
    gsl_vector_set(f,1,gr2);
    return GSL_SUCCESS;
}

int sys_for_gsl(const gsl_vector * x, void * params, gsl_vector * f){
    const double x1=gsl_vector_get(x,0);
    const double x2=gsl_vector_get(x,1);
    double A=10000;
    double f1=A*x1*x2-1;
    double f2=exp(-x1)+exp(-x2)-1-1.0/A;
    gsl_vector_set(f,0,f1);
    gsl_vector_set(f,1,f2);
    return GSL_SUCCESS;
}

int himmelblau_for_gsl(const gsl_vector * x, void * params, gsl_vector * f){
    const double x1=gsl_vector_get(x,0);
    const double x2=gsl_vector_get(x,1);
    double gr1=2*2*x1*(x1*x1+x2-11)+2*(x1+x2*x2-7);
    double gr2=2*(x1*x1+x2-11)+2*2*x2*(x1+x2*x2-7);
    gsl_vector_set(f,0,gr1);
    gsl_vector_set(f,1,gr2);
    return GSL_SUCCESS;
}


int main(){
    gsl_vector* x=gsl_vector_alloc(2);
    gsl_vector* z=gsl_vector_alloc(2);

    gsl_vector* fx=gsl_vector_alloc(2);
    gsl_matrix* J=gsl_matrix_alloc(2,2);
    
    double epsilon=1e-4;//set tolerance
   
    //initialize x
    gsl_vector_set(x,0,10);
    gsl_vector_set(x,1,0.5);
    sys_with_J(x,fx,J);
    //print initial values
    fprintf(stderr,"********** Exercise A **********\n");
    fprintf(stderr,"--Using analytic jacobian--\n\n");
    fprintf(stderr,"System root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton_with_jacobian(sys_with_J,x,epsilon);
    fprintf(stderr,"After %d calls:\n",calls_sys_J);
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    sys_with_J(x,fx,J);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");


    gsl_vector_set(x,0,2);
    gsl_vector_set(x,1,2);
    rosenbrock_with_J(x,fx,J);
    fprintf(stderr,"\n\nRosenbrock root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton_with_jacobian(rosenbrock_with_J,x,epsilon);
    fprintf(stderr,"After %d calls:\n",calls_ros_J);
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    rosenbrock_with_J(x,fx,J);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");

    gsl_vector_set(x,0,2);
    gsl_vector_set(x,1,4);
    himmelblau_with_J(x,fx,J);
    fprintf(stderr,"\n\nHimmelblau root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton_with_jacobian(himmelblau_with_J,x,epsilon);
    fprintf(stderr,"After %d calls:\n",calls_him_J);
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    himmelblau_with_J(x,fx,J);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");


    fprintf(stderr,"\n\n********** EXERCISE B **********\n--Using numerical jacobian--\n\n");
    double dx=1e-6;

    gsl_vector_set(x,0,10);
    gsl_vector_set(x,1,0.5);
    sys(x,fx);
    //print initial values
    fprintf(stderr,"System root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton(sys,x,dx,epsilon);
    fprintf(stderr,"After %d calls:\n",calls_sys);
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    sys(x,fx);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");


    gsl_vector_set(x,0,2);
    gsl_vector_set(x,1,2);
    rosenbrock(x,fx);
    fprintf(stderr,"\n\nRosenbrock root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton(rosenbrock,x,dx,epsilon);
    fprintf(stderr,"After %d calls:\n",calls_ros);
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    rosenbrock(x,fx);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");

    gsl_vector_set(x,0,2);
    gsl_vector_set(x,1,4);
    himmelblau(x,fx);
    fprintf(stderr,"\n\nHimmelblau root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton(himmelblau,x,dx,epsilon);
    fprintf(stderr,"After %d calls:\n",calls_him);
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    himmelblau(x,fx);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");

    fprintf(stderr,"\nCompare with gsl multiroot\n\n");

    fprintf(stderr,"For system root finding:\n");
    gsl_vector_set(x,0,10);
    gsl_vector_set(x,1,0.5);
    gsl_multiroot(sys_for_gsl,x,epsilon);

    fprintf(stderr,"For Rosenbrock root finding:\n");
    gsl_vector_set(x,0,2);
    gsl_vector_set(x,1,2);
    gsl_multiroot(rosenbrock_for_gsl,x,epsilon);
    
    // Note: Wrong root for himmelblau
    fprintf(stderr,"For Himmelblau root finding:\n");
    gsl_vector_set(z,0,2);
    gsl_vector_set(z,1,4);
    gsl_multiroot(himmelblau_for_gsl,z,epsilon);
    
    
    gsl_vector_free(x);
    gsl_vector_free(z);

    gsl_vector_free(fx);
    gsl_matrix_free(J);


    return 0;
}