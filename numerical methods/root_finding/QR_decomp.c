#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_blas.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
    int n=A->size1;
    int m=A->size2;
    
    
    
    for(int i=0;i<m;i++){//for each collumn-vector
    	double inner=0;
        for(int j=0;j<n;j++){//vector elements
		/* double aji=gsl_matrix_get(A,j,i); */
            inner+=gsl_matrix_get(A,j,i)*gsl_matrix_get(A,j,i);
        }
	double rii=sqrt(inner);
        gsl_matrix_set(R,i,i,rii);
        
        for(int j=0;j<n;j++){
            gsl_matrix_set(A,j,i,gsl_matrix_get(A,j,i)/rii);

        }
        for(int j=i+1;j<m;j++){
            double product=0;
            for(int k=0;k<n;k++){
                product += gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
            }
            gsl_matrix_set(R,i,j,product);
            //gsl_matrix_set(R,j,i,0);
            
            
            
            for(int l=0;l<n;l++){
                gsl_matrix_set(A,l,j,gsl_matrix_get(A,l,j)-gsl_matrix_get(A,l,i)*product);
            }
            
        }
    }
    
    
}
void backsub(gsl_matrix *R, gsl_vector *x){
    int n=x->size;
    for(int i=n-1;i>=0;i--){
        double s=0;
        for(int k=i+1;k<n;k++){
            s+=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
        }
	    gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(R,i,i));
            
        
    }
}

void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x){
    
    //QT*b
    gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x);
    
    backsub(R,x);

}

//inverse Gram-Schmidt
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B){
    int m=B->size1;
    //Axi=ei
    
    gsl_vector *e=gsl_vector_calloc(m); //initialize vector elements to 0
    gsl_vector *x=gsl_vector_calloc(m);
    for(int i=0;i<m;i++){
        gsl_vector_set(e,i,1.0);
        qr_gs_solve(Q,R,e,x);
        gsl_vector_set(e,i,0);
        for(int j=0;j<m;j++){
            
            gsl_matrix_set(B,j,i,gsl_vector_get(x,j));
        }
         
    }
    gsl_vector_free(x);
    gsl_vector_free(e);
    

}
