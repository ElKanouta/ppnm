#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

void newton_with_jacobian(void f(gsl_vector* x, gsl_vector* fx,gsl_matrix* J),gsl_vector* x, double epsilon){
    int n=x->size;
//Newton's algorithm
//solve J*Dx=-f(x)  use functions from linear eq. exercise
    gsl_vector* Dx=gsl_vector_alloc(n); //step
    gsl_matrix* J=gsl_matrix_alloc(n,n); //jacobian
    gsl_vector* fx=gsl_vector_alloc(n); //vector f(x)
    gsl_matrix* R=gsl_matrix_alloc(n,n); //upper triangular matrix(for QR)
    // gsl_vector* df=gsl_vector_alloc(n); //derivative
    //define copies of x,fx
    gsl_vector* z=gsl_vector_alloc(n);
    gsl_vector* fz=gsl_vector_alloc(n);
    int steps=0;
    while(1){
        steps++;
        f(x,fx,J);
        qr_gs_decomp(J,R);
    
        qr_gs_solve(J,R,fx,Dx); //found Dx
        gsl_vector_scale(Dx,-1);  //because we have to solve for -f(x)
        //gsl_vector_fprintf(stdout,Dx,"Dx:%g");
        double lambda=1;//initial step
        while(1){
            gsl_vector_memcpy(z,x);//copy x to z
            gsl_vector_add(z,Dx); //now z is x+lambda*Dx
            f(z,fz,J);
            if(gsl_blas_dnrm2(fz)<(1.0-lambda/2.0)*gsl_blas_dnrm2(fx) || lambda<1.0/64.0){
                break;
            } 
            lambda*=0.5; 
            gsl_vector_scale(Dx,0.5); //Dx->lambda*Dx

        }
        gsl_vector_memcpy(x,z);  //x->x+lambda*Dx
        gsl_vector_memcpy(fx,fz);
        if(gsl_blas_dnrm2(fx)<epsilon){
            break;
        }
        
        
    }
    fprintf(stderr,"number of steps:%d\n",steps);
    gsl_vector_free(Dx);
    gsl_matrix_free(J);
    gsl_vector_free(fx);
    gsl_matrix_free(R);
    // gsl_vector_free(df);
    
    


}