#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);


void newton(void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double dx, double epsilon){
	int n=x->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);

	//copies of x,fx
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	int steps=0;
	while(1){
		steps++;
		f(x,fx);
		//define matrix J
		for (int j=0;j<n;j++){
			gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
			f(x,df);
			gsl_vector_sub(df,fx); 
			for(int i=0;i<n;i++) gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
			gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
		}
		//solve J*Dx=-fx
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);//scale because we solve for -fx
		double lambda=1;
		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx); //z=x+Dx
			f(z,fz);
			//Blas norm of vector: dnrm2(vector)
			if( gsl_blas_dnrm2(fz)<(1.0-lambda/2.0)*gsl_blas_dnrm2(fx) || lambda<0.02 ) break;
			lambda*=0.5;
			gsl_vector_scale(Dx,0.5); //Dx->lambda*dx
		}
		gsl_vector_memcpy(x,z);//x is now x+lambda*Dx
		gsl_vector_memcpy(fx,fz);
		if( gsl_blas_dnrm2(Dx)<dx || gsl_blas_dnrm2(fx)<epsilon ) break;
	}
	// fprintf(stderr,"number of steps:%d\n",steps);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(z);
	gsl_vector_free(fz);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
}
