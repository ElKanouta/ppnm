/* Modified version of rosroot.c
from multi-dimensional root finding exercise in practical programming*/
#include<stdio.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>

int gsl_multiroot(int f(gsl_vector* x,void * params,gsl_vector* fx),gsl_vector* x, double epsilon){
    gsl_multiroot_function F;
    F.f=f;
    F.n=2;
    F.params=NULL;
    gsl_multiroot_fsolver * S;
    S=gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,F.n);
    // gsl_vector * start=gsl_vector_alloc(F.n);
    // gsl_vector_set(start,0,gsl_vector_get(x,0));
    // gsl_vector_set(start,1,*y2);
    gsl_multiroot_fsolver_set(S,&F,x);
    
    int flag;
    int iter=0;
    double eps=1e-12;
    do{
        iter++;
        gsl_multiroot_fsolver_iterate(S);
        flag=gsl_multiroot_test_residual(S->f,eps);
        fprintf(stdout,"iteration:%i\tPoint:x=%g, y=%g\tfx=%g, fy=%g\n",iter,gsl_vector_get(S->x,0),gsl_vector_get(S->x,1),gsl_vector_get(S->f,0),gsl_vector_get(S->f,1));


    }
    while(flag==GSL_CONTINUE);
    fprintf(stderr,"iteration:%i\tPoint:x=%g, y=%g\tfx=%g, fy=%g\n\n",iter,gsl_vector_get(S->x,0),gsl_vector_get(S->x,1),gsl_vector_get(S->f,0),gsl_vector_get(S->f,1));

    //points of minimum
    gsl_vector_get(S->x,0);
    gsl_vector_get(S->x,1);
    
    gsl_multiroot_fsolver_free(S);
    return 0;

}
// int main(){
//     double *rootx, *rooty, x, y;
//     //initialize
//     x=0.5;
//     y=0.5;
//     rootx=&x;
//     rooty=&y;
//     printf("Exercise 1: Gradient of rosenbrock function\n");
//     find_root(rootx,rooty);
//     return 0;
// }