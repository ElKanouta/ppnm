#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

double newton(double f(gsl_vector* x,gsl_vector* df, gsl_matrix* H),gsl_vector* xstart, double epsilon){
    int n=xstart->size;
    gsl_matrix* H=gsl_matrix_alloc(n,n);
    gsl_vector* Dx=gsl_vector_alloc(n);
    gsl_vector* df=gsl_vector_alloc(n);
    gsl_matrix* R=gsl_matrix_alloc(n,n);

    //define copies of x,df
    gsl_vector* z=gsl_vector_alloc(n);
    gsl_vector* dfz=gsl_vector_alloc(n);
    gsl_vector* s=gsl_vector_alloc(n);
    
    double alpha=1e-4;
    double result;
    double steps=0;
    
    //function output
    double fx=f(xstart,df,H);
    double fz;
    
    //solve H*Dx=-df
    while(1){
        qr_gs_decomp(H,R);
        qr_gs_solve(H,R,df,Dx);
        gsl_vector_scale(Dx,-1);
        double lambda=1;//initial step

        while(1){
            for(int i=0;i<n;i++){
                gsl_vector_set(s,i,lambda*gsl_vector_get(Dx,i));
            }
            gsl_vector_memcpy(z,xstart);//copy x to z
            gsl_vector_add(z,Dx); //now z is x+lambda*Dx
            f(z,dfz,H);
            gsl_blas_ddot(s,df,&result);
            if(f(z,dfz,H)<f(xstart,df,H)+alpha*result || lambda<0 ){
                break;
            } 
            lambda*=0.5; 
            gsl_vector_scale(Dx,0.5); //Dx->lambda*Dx
            fz=f(z,dfz,H);
        }
        gsl_vector_memcpy(xstart,z);  //x->x+lambda*Dx
        gsl_vector_memcpy(df,dfz);
        if(gsl_blas_dnrm2(df)<epsilon ){
            break;
        }
        fx=fz;
        steps++; 
    }

    gsl_matrix_free(H);
    gsl_matrix_free(R);
    gsl_vector_free(Dx);
    gsl_vector_free(df);
    gsl_vector_free(s);
    return steps;    
}