#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
/*Quasi-Newton metho d with symmetric Broyden's update*/


double quasi_newton(double f(gsl_vector* x, gsl_vector* df),gsl_vector* x, double epsilon){
    double steps=0;
    int n=x->size;
    //gsl_matrix* H=gsl_matrix_alloc(n,n);
    gsl_matrix* B=gsl_matrix_alloc(n,n);// inverse hessian
    gsl_vector* df=gsl_vector_alloc(n);
    gsl_vector* y=gsl_vector_calloc(n);
    //gsl_vector* s=gsl_vector_alloc(n);
    gsl_vector* u=gsl_vector_alloc(n);
    gsl_vector* Dx=gsl_vector_alloc(n);
    gsl_vector* alpha=gsl_vector_alloc(n);
    
    double sTy,sTdf,uTy;
    double lambda;
    double fx=f(x,df);
    
    //copies of x,df
    gsl_vector* z=gsl_vector_alloc(n);
    gsl_vector* dfz=gsl_vector_alloc(n);
    double fz;
    
    gsl_matrix_set_identity(B);
    while(gsl_blas_dnrm2(df)>epsilon){
        lambda=1;
        //printf("1\n");
        steps++;
        gsl_blas_dgemv(CblasNoTrans,-1,B,df,0,Dx); //Dx=-B*df
        //gsl_blas_daxpy(lambda,Dx,s);//s=lambda*Dx
        while(1){
            gsl_vector_memcpy(z,x);//copy x to z
            gsl_vector_add(z,Dx); //now z is x+lambda*Dx
            fz=f(z,dfz);
            //printf("%g\n",fz);
            gsl_blas_ddot(Dx,df,&sTdf);
            //printf("%g\n",sTdf);
            if(fz<fx+0.01*sTdf || lambda<epsilon){
                break;
            }
            //printf("2\n");
            lambda*=0.5;
            //gsl_blas_daxpy(lambda,Dx,s);
            gsl_vector_scale(Dx,0.5);
            
        }
        //printf("3\n");
        // for(int i=0;i<n;i++){
        //     gsl_vector_set(y,i,gsl_vector_get(dfz,i)-gsl_vector_get(df,i));
        // }
        gsl_vector_memcpy(y, dfz); 
		gsl_blas_daxpy(-1.0, df, y);

        //gsl_vector_fprintf(stdout,y,"%g");
        //u=s-By
        gsl_blas_dgemv(CblasNoTrans,-1,B,y,0,u);
        gsl_vector_add(u,Dx);
        //printf("4\n");
        
        gsl_blas_ddot(Dx,y,&sTy);//sTy
        if(fabs(sTy)>epsilon){
            //printf("5\n");

            gsl_blas_ddot(u,y,&uTy);
            double gamma=uTy/(2*sTy);
            gsl_blas_daxpy(-gamma,Dx,alpha);
            gsl_vector_add(alpha,u);
            gsl_vector_scale(alpha,1.0/sTy);
            //update matrix
            gsl_blas_dger(1.0/sTy,u,Dx,B);
			gsl_blas_dger(1.0/sTy,Dx,u,B);
        

        }
        gsl_vector_memcpy(x,z);  
        gsl_vector_memcpy(df,dfz);
        fx=fz;
        //gsl_vector_fprintf(stdout,x,"%g");
        
    }
    
    gsl_matrix_free(B);
    gsl_vector_free(df);
    gsl_vector_free(y);
    //gsl_vector_free(s);
    gsl_vector_free(u);
    gsl_vector_free(z);
    gsl_vector_free(dfz);
    gsl_vector_free(Dx);
    gsl_vector_free(alpha);
    return steps;
}