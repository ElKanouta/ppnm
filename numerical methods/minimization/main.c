#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

double newton(double f(gsl_vector* x,gsl_vector* df, gsl_matrix* H),gsl_vector* xstart, double epsilon);
int qnewton(double f(gsl_vector* x),gsl_vector* x, double epsilon);
void newton_with_jacobian(void f(gsl_vector* x, gsl_vector* fx,gsl_matrix* J),gsl_vector* x, double epsilon);

int downhill_simplex(double F(double*), double** simplex, int d, double simplex_size_goal);


double rosenbrock(gsl_vector*xi, gsl_vector* df, gsl_matrix* H){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(df,0,2*(1-x)*(-1)-100*2*2*x*(y-x*x)); //derivative over x
    gsl_vector_set(df,1,100*2*(y-x*x)); //derivative over y
    gsl_matrix_set(H,0,0,2-100*4*(y-x*x)+100*8*x*x);
    gsl_matrix_set(H,0,1,-4*100*x);
    gsl_matrix_set(H,1,0,-4*100*x);
    gsl_matrix_set(H,1,1,2*100);
    
    return (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
}

double rosenbrock_q(gsl_vector*xi){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    // gsl_vector_set(df,0,2*(1-x)*(-1)-100*2*2*x*(y-x*x)); //derivative over x
    // gsl_vector_set(df,1,100*2*(y-x*x)); //derivative over y
    
    return (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
}

double himmelblau(gsl_vector* xi,gsl_vector* df,gsl_matrix* H){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(df,0,2*2*x*(x*x+y-11)+2*(x+y*y-7)); //derivative over x
    gsl_vector_set(df,1,2*(x*x+y-11)+2*2*y*(x+y*y-7)); //derivative over y
    gsl_matrix_set(H,0,0,4*(x*x+y-11)+4*x*2*x+2);
    gsl_matrix_set(H,0,1,4*x+2*2*y);
    gsl_matrix_set(H,1,0,2*2*x+2*2*y);
    gsl_matrix_set(H,1,1,2+4*2*y);
    
    return (x*x+y-11)*(x*x+y-11)+(x+y*y-7)*(x+y*y-7);
}

double himmelblau_q(gsl_vector* xi){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    // gsl_vector_set(df,0,2*2*x*(x*x+y-11)+2*(x+y*y-7)); //derivative over x
    // gsl_vector_set(df,1,2*(x*x+y-11)+2*2*y*(x+y*y-7)); //derivative over y
    
    return (x*x+y-11)*(x*x+y-11)+(x+y*y-7)*(x+y*y-7);
}

void rosenbrock_with_J(gsl_vector* xi, gsl_vector* fx,gsl_matrix* J){ //roots of gradient
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(fx,0,2*(1-x)*(-1)-100*2*2*x*(y-x*x)); //derivative over x
    gsl_vector_set(fx,1,100*2*(y-x*x)); //derivative over y
    gsl_matrix_set(J,0,0,2-100*4*(y-x*x)+100*8*x*x);
    gsl_matrix_set(J,0,1,-4*100*x);
    gsl_matrix_set(J,1,0,-4*100*x);
    gsl_matrix_set(J,1,1,2*100);
    
}

void himmelblau_with_J(gsl_vector* xi,gsl_vector* fx,gsl_matrix* J){
    double x=gsl_vector_get(xi,0);
    double y=gsl_vector_get(xi,1);
    gsl_vector_set(fx,0,2*2*x*(x*x+y-11)+2*(x+y*y-7)); //derivative over x
    gsl_vector_set(fx,1,2*(x*x+y-11)+2*2*y*(x+y*y-7)); //derivative over y
    gsl_matrix_set(J,0,0,4*(x*x+y-11)+4*x*2*x+2);
    gsl_matrix_set(J,0,1,4*x+2*2*y);
    gsl_matrix_set(J,1,0,2*2*x+2*2*y);
    gsl_matrix_set(J,1,1,2+4*2*y);
    
}

double fitting_func(gsl_vector* x){
    //fitting parameters
    double A=gsl_vector_get(x,0); //initial amount
    double T=gsl_vector_get(x,1); //lifetime
    double B=gsl_vector_get(x,2); //background noise

    //data
    double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
    double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
    double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
    int N = sizeof(t)/sizeof(t[0]);

    double F = 0;
    double f=0;
	for(int i=0;i<N;i++){
        f=A*exp(-t[i]/T)+B;
        F += pow(f-y[i],2)/pow(e[i],2);
    } 
    return F; //master function

}

double rosenbrock_simplex(double* z){
    double x=z[0];
    double y=z[1];
    return (1-x)*(1-x)+100*(y-x*x)*(y-x*x);
}

double himmelblau_simplex(double* z){
    double x=z[0];
    double y=z[1];
    return (x*x+y-11)*(x*x+y-11)+(x+y*y-7)*(x+y*y-7);
}

double fitting_func_simplex(double* x){
    //fitting parameters
    double A=x[0]; //initial amount
    double T=x[1]; //lifetime
    double B=x[2]; //background noise

    //data
    double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
    double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
    double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
    int N = sizeof(t)/sizeof(t[0]);

    double F = 0;
    double f=0;
	for(int i=0;i<N;i++){
        f=A*exp(-t[i]/T)+B;
        F += pow(f-y[i],2)/pow(e[i],2);
    } 
    return F; //master function

}

int main(){
    gsl_vector* xstart=gsl_vector_alloc(2);
    gsl_vector* df=gsl_vector_alloc(2);
    gsl_matrix* H=gsl_matrix_alloc(2,2);
    
    double epsilon=1e-6;//set tolerance
    gsl_vector_set(xstart,0,2);
    gsl_vector_set(xstart,1,2);
    fprintf(stderr,"\n********** EXERCISE A **********\n\n");
    fprintf(stderr,"Rosenbrock minima\nInitial guess for x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%g");
    // fprintf(stderr,"df initial\n");
    // gsl_vector_fprintf(stderr,df,"%g");
    double no_of_steps_ros=newton(rosenbrock,xstart,epsilon);
    
    fprintf(stderr,"solution x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%g");
    
    fprintf(stderr,"Check:df should be 0\n");
    gsl_vector_fprintf(stderr,df,"%g");
    fprintf(stderr,"Number of steps: %g\n",no_of_steps_ros);

    
    
    gsl_vector_set(xstart,0,2.8);
    gsl_vector_set(xstart,1,2.5);

    fprintf(stderr,"\n\nHimmelblau minima\nInitial guess for x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%g");
    
    double no_of_steps_him=newton(himmelblau,xstart,epsilon);
    
    fprintf(stderr,"solution x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%g");
    
    fprintf(stderr,"Check:df should be 0\n");
    gsl_vector_fprintf(stderr,df,"%g");
    fprintf(stderr,"Number of steps: %g\n",no_of_steps_him);


    //***************************************************************//
    gsl_vector_set(xstart,0,2);
    gsl_vector_set(xstart,1,2);
    fprintf(stderr,"\n********** EXERCISE B **********\nQuasi-Newton method with symmetric Broyden's update\n\n");
    fprintf(stderr,"Rosenbrock minima\nInitial guess for x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%g");
    // fprintf(stderr,"df initial\n");
    // gsl_vector_fprintf(stderr,df,"%g");
    double no_of_steps_ros_q=qnewton(rosenbrock_q,xstart,epsilon);
    
    fprintf(stderr,"solution x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%.3g");
    
    fprintf(stderr,"Check:df should be 0:  %g\n",rosenbrock_q(xstart));
    
    //gsl_vector_fprintf(stderr,df,"%g");
    fprintf(stderr,"Number of steps: %g\n",no_of_steps_ros_q);

    
    
    gsl_vector_set(xstart,0,2.5);
    gsl_vector_set(xstart,1,3.5);

    fprintf(stderr,"\n\nHimmelblau minima\nInitial guess for x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%g");
    
    double no_of_steps_him_q=qnewton(himmelblau_q,xstart,epsilon);
    
    fprintf(stderr,"solution x,y:\n");
    gsl_vector_fprintf(stderr,xstart,"%g");
    
    fprintf(stderr,"Check:df should be 0:  %g\n",himmelblau_q(xstart));
    
    fprintf(stderr,"Number of steps: %g\n",no_of_steps_him_q);

    //compare with root finding exercise to quasi newton with derivatives
    fprintf(stderr,"\nComparison with root finding:");
    gsl_vector* x=gsl_vector_alloc(2);
    gsl_vector* fx=gsl_vector_alloc(2);
    gsl_matrix* J=gsl_matrix_alloc(2,2);

    gsl_vector_set(x,0,2);
    gsl_vector_set(x,1,2);
    rosenbrock_with_J(x,fx,J);
    fprintf(stderr,"\n\nRosenbrock root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton_with_jacobian(rosenbrock_with_J,x,epsilon);
    
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    rosenbrock_with_J(x,fx,J);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");

    gsl_vector_set(x,0,2);
    gsl_vector_set(x,1,4);
    himmelblau_with_J(x,fx,J);
    fprintf(stderr,"\n\nHimmelblau root finding\nInitial vectors:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    fprintf(stderr,"f(x) initial\n");
    gsl_vector_fprintf(stderr,fx,"%g");
    newton_with_jacobian(himmelblau_with_J,x,epsilon);
    
    fprintf(stderr,"solution x:\n");
    gsl_vector_fprintf(stderr,x,"%g");
    himmelblau_with_J(x,fx,J);
    fprintf(stderr,"f(x)\n");
    gsl_vector_fprintf(stderr,fx,"%g");

//least squares
    //data
    double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
    double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
    double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
    int N = sizeof(t)/sizeof(t[0]);

    //print data
    for(int i=0;i<N;i++){
        fprintf(stdout,"%g %g %g\n",t[i],y[i],e[i]);
    }
    fprintf(stdout,"\n\n");

    //initial values for fitting
    gsl_vector* xF=gsl_vector_alloc(3);
    gsl_vector_set(xF,0,5.0); //guess for A
    gsl_vector_set(xF,1,2.0); //guess for T
    gsl_vector_set(xF,2,2.0); //guess for B
    fprintf(stderr,"\n\nFitting minimization problem:\n");
    fprintf(stderr,"Initial guess for A,T,B:\n");
    gsl_vector_fprintf(stderr,xF,"%g");
    int steps=qnewton(fitting_func,xF,epsilon);
    fprintf(stderr,"Solutions for A,T,B:\n");
    gsl_vector_fprintf(stderr,xF,"%g");
    fprintf(stderr,"Number of steps:%d\n",steps);

    //print fit
    for(double i=0;i<=10;i+=0.1){
        fprintf(stdout,"%g %g\n",i,gsl_vector_get(xF,0)*exp(-i/gsl_vector_get(xF,1))+gsl_vector_get(xF,2));
    }

    fprintf(stderr,"********** EXERCISE C **********\nDownhill simplex method\n\n");
    int dim=2;
    //Rosenbrock
    //allocate simplex matrix (n+1) x n
    double** simplex=(double**)calloc(dim+1,sizeof(double*)); //rows
    for(int i=0;i<dim+1;i++){
        simplex[i]=(double*)calloc(dim,sizeof(double));
    }

    fprintf(stderr,"Rosenbrock minima\nInitial guess for simplex:\n");
    //initialize random simplex
    for(int i=0;i<dim+1;i++){
        
        for(int j=0;j<dim;j++){
            double randnum=((double)rand())/((double)RAND_MAX);
            simplex[i][j]=randnum;
            fprintf(stderr,"%g ",simplex[i][j]);
        }
        fprintf(stderr,"\n");
    }
    fprintf(stderr,"\n\n");

    downhill_simplex(rosenbrock_simplex,simplex,dim,epsilon);
    fprintf(stderr,"Simplex after minimization:\n");
    for(int i=0;i<dim+1;i++){
        for(int j=0;j<dim;j++){
            fprintf(stderr,"%g ",simplex[i][j]);
        }
        fprintf(stderr,"\n");
    }
    fprintf(stderr,"\n\n");


    //Himmelblau
    double** simplex_H=(double**)calloc(dim+1,sizeof(double*)); //rows
    for(int i=0;i<dim+1;i++){
        simplex_H[i]=(double*)calloc(dim,sizeof(double));
    }
     fprintf(stderr,"Himmelblau minima\nInitial guess for simplex:\n");
    //initialize random simplex
    for(int i=0;i<dim+1;i++){
        
        for(int j=0;j<dim;j++){
            double randnum=((double)rand())/((double)RAND_MAX);
            simplex_H[i][j]=randnum;
            fprintf(stderr,"%g ",simplex_H[i][j]);
        }
        fprintf(stderr,"\n");
    }
    fprintf(stderr,"\n\n");

    downhill_simplex(himmelblau_simplex,simplex_H,dim,epsilon);
    fprintf(stderr,"Simplex after minimization:\n");
    for(int i=0;i<dim+1;i++){
        for(int j=0;j<dim;j++){
            fprintf(stderr,"%g ",simplex_H[i][j]);
        }
        fprintf(stderr,"\n");
    }
    fprintf(stderr,"\n\n");


    //fitting function
    int dim_fit=3;
    double** simplex_fit=(double**)calloc(dim_fit+1,sizeof(double*)); //rows
    for(int i=0;i<dim_fit+1;i++){
        simplex_fit[i]=(double*)calloc(dim,sizeof(double));
    }
     fprintf(stderr,"Fitting minimization problem\nInitial guess for simplex:\n");
    //initialize random simplex
    for(int i=0;i<dim_fit+1;i++){
        
        for(int j=0;j<dim_fit;j++){
            double randnum=((double)rand())/((double)RAND_MAX);
            simplex_fit[i][j]=randnum;
            fprintf(stderr,"%g ",simplex_fit[i][j]);
        }
        fprintf(stderr,"\n");
    }
    fprintf(stderr,"\n\n");

    downhill_simplex(fitting_func_simplex,simplex_fit,dim_fit,epsilon);
    fprintf(stderr,"Simplex after minimization:\n");
    for(int i=0;i<dim_fit+1;i++){
        for(int j=0;j<dim_fit;j++){
            fprintf(stderr,"%g ",simplex_fit[i][j]);
        }
        fprintf(stderr,"\n");
    }



    





    gsl_vector_free(xstart);
    gsl_vector_free(df);
    gsl_matrix_free(H);

    gsl_vector_free(x);
    gsl_vector_free(xF);
    gsl_vector_free(fx);
    gsl_matrix_free(J);
    free(simplex);
    free(simplex_H);

    return 0;
}


