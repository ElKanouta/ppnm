#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#define RND ((double)rand()/RAND_MAX)

//define random point between and b
void randomx(int dim,double *a, double *b, double *x){
    for(int i=0;i<dim;i++){
        x[i]=a[i]+RND*(b[i]-a[i]);
    }
}


void plainmc(int dim, double *a, double *b, double f(double* x),int N, double *result, double *error){
    double V=1;
    for(int i=0;i<dim;i++){
        V*=b[i]-a[i];
    }
    double sum=0; //sum of f
    double sum2=0; //sum of f^2
    double fx, x[dim];
    for(int i=0;i<N;i++){
        randomx(dim,a,b,x);
        fx=f(x);
        sum+=fx;
        sum2+=fx*fx;
    }
    double avr=sum/N;
    double var=sum2/N-avr*avr; //variance <f^2>-<f>^2
    *result=avr*V; //V*<f>
    *error=sqrt(var/N)*V; //statistical error
}