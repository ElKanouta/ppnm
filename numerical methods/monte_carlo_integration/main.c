#include<stdio.h>
#include<math.h>
#define M_PI 3.14159
void plainmc(int dim, double *a, double *b, double f(double* x),int N, double *result, double *error);

double test_func_1(double* x){
    return sqrt(*x+sqrt(*x));
}

double test_func_2(double* x){
    return 4-x[0]*x[0]-x[1]*x[1];
}

double test_func_3(double* x){
    return 4-x[0]*x[0]-x[1]*x[1]-x[2]*x[2];
}

double test_func_4(double* x){
    return pow(1-cos(x[0])*cos(x[1])*cos(x[2]),-1)/M_PI/M_PI/M_PI;
}


int main(){
    printf("\nExercise A\nPlain Monte Carlo integration\n\n");
    printf("Integrate sqrt(x+sqrt(x)) from 0 to 1\n");
    double a=0;
    double b=1;
    double result,error;
    plainmc(1,&a,&b,test_func_1,1000,&result,&error);
    printf("Result=%g\nError=%g\n",result,error);
    printf("---Exact result:1.0453\n");

    printf("\nIntegrate f(x,y)=4-x^2-y^2 from 0 to 5/4 on dx and dy\n");
    double c[2], d[2];
    c[0]=0, c[1]=0;
    d[0]=5.0/4.0, d[1]=5.0/4.0;
    plainmc(2,&c,&d,test_func_2,1000,&result,&error);
    printf("Result=%g\nError=%g\n",result,error);
    printf("---Exact result:4.6224\n");

// //does not work 
//     printf("\nIntegrate f(x,y,z)=4-x^2-y^2-z^2 from 0 to 11/10 on dz, 0 to 1 on dy and 0 to 9/10 on dx \n");
//     double e[3], f[3];
//     e[0]=0, e[1]=0, e[2]=0;
//     d[0]=11.0/10.0 ,d[1]=1.0, d[2]=9.0/10.0 ;
//     plainmc(3,&e,&f,test_func_3,1000,&result,&error);
//     printf("Result=%g\nError=%g\n",result,error);

    printf("\nIntegrate f(x,y,z)=(1-cos(x)*cos(y)*cos(z))^(-1) from 0 to pi on dx, dy and dz\n");
    double g[3], h[3];
    g[0]=0, g[1]=0, g[2]=0;
    h[0]=M_PI, h[1]=M_PI, h[2]=M_PI;
    plainmc(3,&g,&h,test_func_4,1000,&result,&error);
    printf("Result=%g\nError=%g\n",result,error);
    printf("---Exact result:1.3932039296856768591842462603255\n");


    //Exercise B
    printf("\n\nExercise B\nError estimate\n");
    printf("\nWe find the result and error by estimating the first integral again for different N\nError must be O(1/sqrt(N))\n");
    printf("The errors are written in error.txt and plots in plot.svg\n");
    
    for(int i=100;i<=100000;i*=10){
        plainmc(1,&a,&b,test_func_1,i,&result,&error);
        fprintf(stderr,"%d  %g\n",i,error);
    }
    
    return 0;
}


