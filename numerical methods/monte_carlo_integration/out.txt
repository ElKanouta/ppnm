
Exercise A
Plain Monte Carlo integration

Integrate sqrt(x+sqrt(x)) from 0 to 1
Result=1.05484
Error=0.00837443
---Exact result:1.0453

Integrate f(x,y)=4-x^2-y^2 from 0 to 5/4 on dx and dy
Result=4.634
Error=0.0328828
---Exact result:4.6224

Integrate f(x,y,z)=(1-cos(x)*cos(y)*cos(z))^(-1) from 0 to pi on dx, dy and dz
Result=1.33661
Error=0.0498546
---Exact result:1.3932039296856768591842462603255


Exercise B
Error estimate

We find the result and error by estimating the first integral again for different N
Error must be O(1/sqrt(N))
The errors are written in error.txt and plots in plot.svg
