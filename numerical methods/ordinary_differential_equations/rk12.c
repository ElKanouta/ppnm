#include<stdio.h>
#include<math.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>


//stepper:advances the solution by one step and estimates the error
void rkstep12(double t, double h,gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err){
    int n=yt->size;
    // gsl_vector* y=gsl_vector_alloc(n);
    // gsl_vector* dydt=gsl_vector_alloc(n);
    //gsl_vector* yt_next=gsl_vector_alloc(n);


    gsl_vector* k0=gsl_vector_alloc(n);
    gsl_vector* k12=gsl_vector_alloc(n);
    f(t,yt,k0);
    gsl_vector_memcpy(k12,yt);
    for(int i=0;i<n;i++){
        gsl_vector_set(k12,i,gsl_vector_get(yt,i)+gsl_vector_get(k0,i)*h/2);
    }
    f(t+h/2,k12,k12);
    gsl_vector_memcpy(yth,yt);
    for(int i=0;i<n;i++){
        gsl_vector_set(yth,i,gsl_vector_get(yt,i)+gsl_vector_get(k12,i)*h);
    }
    for(int i=0;i<n;i++){
        gsl_vector_set(err,i,(gsl_vector_get(k0,i)-gsl_vector_get(k12,i))*h/2);
    }


    // gsl_vector_free(y);
    // gsl_vector_free(dydt);
    gsl_vector_free(k0);
    gsl_vector_free(k12);
    //gsl_vector_free(yt_next);

}


//driver:monitors the local errors and tolerances andadjusts  the  step-sizes
void driver(double* t,double b,double* h,gsl_vector* yt,double acc, double eps,void stepper(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth,gsl_vector* err),void f(double t, gsl_vector* yt,gsl_vector* dydt) ){
       int n=yt->size;

    gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	double a = *t; //left limit
    double tau; //tolerance
    double t0; //starting point
    double s; //step
	int steps = 0; //number of steps
	while (steps<1000) {
		fprintf(stderr,"%i\n",steps);
		t0 = *t;
		s = *h;
		if (t0>=b) { //starting point out of range
            break;
        }
		if (t0+s > b){
           s = b-t0; 
        } 
		stepper(t0, s, yt, f, yth, err); //advance the solution by s, estimate err

		double err_norm = gsl_blas_dnrm2(err);
		tau = (acc +gsl_blas_dnrm2(yt)*eps)*sqrt(s/(b-a));
		 
		if (err_norm<tau) { //accept step
			steps++;
			*t = t0+s;
			gsl_vector_memcpy(yt,yth);
            //store path
			printf("%g %g %g\n",*t, gsl_vector_get(yt,0),gsl_vector_get(yt,1));
			
		}
        // update stepsize
		if (err==0){
            *h *=2;
        }
		else {
            *h *= pow(tau/err_norm,0.25)*0.95;
        }
	}
    
	gsl_vector_free(yth);
	gsl_vector_free(err);
}