#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>

void driver(double* t,double b,double* h,gsl_vector* yt,double acc, double eps,void stepper(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth,gsl_vector* err),void f(double t, gsl_vector* yt,gsl_vector* dydt) );
void rkstep12(double t, double h,gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_vector* yth, gsl_vector* err);

//TO DO:check results and find path 
void func(double t, gsl_vector* y, gsl_vector* dydt){//trigonometric functions
    double y0=gsl_vector_get(y,0);
    double y1=gsl_vector_get(y,1);
    gsl_vector_set(dydt,0,y1);
    gsl_vector_set(dydt,1,-y0);
}


int main(){
    int n=2;
    gsl_vector* y=gsl_vector_alloc(n);
    gsl_vector_set(y,0,0);
    gsl_vector_set(y,1,1);
    double a=-0;
    double b=4*M_PI;
    double acc=1e-2;
    double eps=1e-2;
    double s=0.05;
    
    driver(&a,b,&s,y,acc,eps,rkstep12,func);
    
    gsl_vector_free(y);
    return 0;
}

