#include<stdio.h>
#include<stdlib.h>
#include"linterp.h"
#include<math.h>
#include"qspline.h"
int main(){
    int n=6;
    double interval=20/(n-1);
    double x[n], y[n];
    //linterp
    for(int i=0;i<n;i++){
        x[i]=i*interval;
        y[i]=pow(i*interval,2);
    }
    for(double z=0.2;z<=20;z+=0.2){
        fprintf(stdout,"%g %g %g %g %g\n",z,linterp(n,x,y,z),pow(z,2),linterp_integ(n,x,y,z),(1.0/3.0)*pow(z,3));
        
    }
    //qspline
    qspline *s=qspline_alloc(n,x,y);
    for(double z=0.2;z<=20;z+=0.2){
        fprintf(stderr,"%g %g %g %g %g %g %g\n",z,qspline_evaluate(s,z),pow(z,2),qspline_integral(s,z),(1.0/3.0)*pow(z,3),qspline_derivative(s,z),2*z);
    
    }
    qspline_free(s);
//check for sin(x)
    // double interval=2*M_PI/(n-1);
    // for(int i=0;i<n;i++){
    //     x[i]=i*interval;
    //     y[i]=sin(i*interval);
    // }

    // qspline *s=qspline_alloc(n,x,y);
    // for(double z=0.2;z<=20;z+=0.2){
    //     fprintf(stderr,"%g %g %g %g %g %g %g\n",z,qspline_evaluate(s,z),sin(z),qspline_integral(s,z),1-cos(z),qspline_derivative(s,z),cos(z));
    
    // }
    // qspline_free(s);

    return 0;
}