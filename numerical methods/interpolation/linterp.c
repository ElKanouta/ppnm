#include<assert.h>

double linterp (int n,double* x,double* y, double z){
    assert(n>1 && z>=x[0] && z<=x[n-1]);
    int i=0, j=n-1;
    while(j-i>1){
        int m=(i+j)/2;
        if(z>x[m]){
            i=m;
        }
        else{
            j=m;
        }
    }
    return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}

double linterp_integ(int n, double* x, double* y, double z){
    assert(n>1 && z>=x[0] && z<=x[n-1]);
    //binary search
    int i=0, j=n-1;
    while(j-i>1){
        int m=(i+j)/2;
        if(z>x[m]){
            i=m;
        }
        else{
            j=m;
        }
    }
    double sum=0, integral=0, rest=0;
    int l=0;
    //for(int l=0;l<j;l++){
    while(l<=i){
        if(l==i){
            integral= y[l]*z + (y[l+1]-y[l])/(x[l+1]-x[l])*(0.5*z*z-x[l]*z)- y[l]*x[l]-(y[l+1]-y[l])/(x[l+1]-x[l])*(0.5*x[l]*x[l]-x[l]*x[l]);
            // integral =y[l]*(z-x[l])+0.5*((y[l+1]-y[l])/(x[l+1]-x[l]))*(z-x[l])*(x[l+1]-x[l]);
            sum+=integral;
        }
        else{
            rest = y[l]*x[l+1] + (y[l+1]-y[l])/(x[l+1]-x[l])*(0.5*x[l+1]*x[l+1]-x[l]*x[l+1])- y[l]*x[l]-(y[l+1]-y[l])/(x[l+1]-x[l])*(0.5*x[l]*x[l]-x[l]*x[l]);
            //rest=y[i]*(z-x[i])+0.5*((y[l+1]-y[l])/(x[l+1]-x[l]))*(x[i+1]-x[i])*(x[i+1]-x[i]);
            sum+=rest;
        }
    l++;
    }
    return sum;
}

