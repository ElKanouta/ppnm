#include<stdlib.h>
#include<assert.h>

typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline* qspline_alloc(int n, double* x, double* y){
    qspline *s=(qspline*)malloc(sizeof(qspline));
    s->b = (double*)malloc((n-1)*sizeof(double));
    s->c = (double*)malloc((n-1)*sizeof(double));
    s->x = (double*)malloc(n*sizeof(double));
    s->y = (double*)malloc(n*sizeof(double));
    s->n = n;
    for(int i=0;i<n;i++){
        s->x[i]=x[i];
        s->y[i]=y[i];
    }
    int i;
    double p[n-1], h[n-1];
    for(i=0;i<n-1;i++){
        h[i]=x[i+1]-x[i];
        p[i]=(y[i+1]-y[i])/h[i];
    }
    s->c[0]=0;
    for(i=0;i<n-2;i++){
        s->c[i+1]=(p[i+1]-p[i]-s->c[i]*h[i])/h[i+1];
        s->c[n-2]/=2;
    }
    for(i=n-3;i>=0;i--){
        s->c[i]=(p[i+1]-p[i]-s->c[i+1]*h[i+1])/h[i];
    }
    for(i=0;i<n-1;i++){
        s->b[i]=p[i]-s->c[i]*h[i];
    }
    return s;
}

double qspline_evaluate(qspline *s, double z){
    assert(z>=s->x[0] && z<=s->x[s->n-1]);
    int i=0, j=s->n-1;
    while(j-i>1){
        int m=(i+j)/2;
        if(z>s->x[m]){
            i=m;
        }
        else{
            j=m;
        }
    }
    double h=z-s->x[i];
    return s->y[i]+h*(s->b[i]+h*s->c[i]);
    
}

double qspline_derivative(qspline *s, double z){
    assert(z>=s->x[0] && z<=s->x[s->n-1]);
    int i=0, j=s->n-1;
    while(j-i>1){
        int m=(i+j)/2;
        if(z>s->x[m]){
            i=m;
        }
        else{
            j=m;
        }
    }
    return s->b[i] +s->c[i]*2*(z-s->x[i]);
}

double qspline_integral(qspline *s, double z){
    assert(z>=s->x[0] && z<=s->x[s->n-1]);
    int i=0, j=s->n-1;
    while(j-i>1){
        int m=(i+j)/2;
        if(z>s->x[m]){
            i=m;
        }
        else{
            j=m;
        }
    }
    int l=0;
    double integral=0;
    while(l<=i){
        if(l==i){
            integral+=s->y[l]*z+s->b[l]*(z*z/2-s->x[l]*z)+s->c[l]*(z*z*z/3-z*z*s->x[l]+z*s->x[l]*s->x[l])-(s->y[l]*s->x[l]+s->b[l]*(s->x[l]*s->x[l]/2-s->x[l]*s->x[l])+s->c[l]*(s->x[l]*s->x[l]*s->x[l]/3));

        }
        else{
            integral+=s->y[l]*s->x[l+1]+s->b[l]*(s->x[l+1]*s->x[l+1]/2-s->x[l]*s->x[l+1])+s->c[l]*(s->x[l+1]*s->x[l+1]*s->x[l+1]/3-s->x[l+1]*s->x[l+1]*s->x[l]+s->x[l+1]*s->x[l]*s->x[l])-(s->y[l]*s->x[l]+s->b[l]*(s->x[l]*s->x[l]/2-s->x[l]*s->x[l])+s->c[l]*(s->x[l]*s->x[l]*s->x[l]/3));
        }
        l++;
    }
    return integral;

}


void qspline_free(qspline *s){
    free(s->x);
    free(s->y);
    free(s->b);
    free(s->c);
    free(s);
}
