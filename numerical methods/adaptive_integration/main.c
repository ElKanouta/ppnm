#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>

double adapt(double f(double), double a, double b, double acc, double eps,double *err);
double clenshaw_curtis(double f(double),double a,double b,double acc,double eps,double *err);


int calls_1=0;
int calls_2=0;
int calls_3=0;
int calls_4=0;
int calls_5=0;
int calls_6=0;
int calls_7=0;

double square_root(double x){
    calls_1++;
    return sqrt(x);
}

double one_over_sqrt(double x){
    calls_2++;
    return 1.0/sqrt(x);
}

double one_over_sqrt_gsl(double x,void* params){
    
    return 1.0/sqrt(x);
}

double ln_over_sqrt(double x){
    calls_3++;
    return log(x)/sqrt(x);
}

double ln_over_sqrt_gsl(double x,void* params){
    
    return log(x)/sqrt(x);
}

double func(double x){
    calls_4++;
    return 4*sqrt(1-(1-x)*(1-x));
}

double func2(double x){
    calls_5++;
    return x*exp(-x*x);
}

double func2_gsl(double x,void* params){
    
    return x*exp(-x*x);
}

double func3(double x){
    calls_6++;
    return 1/(x*x+1);
}

double func3_gsl(double x,void* params){
    
    return 1/(x*x+1);
}

double func4(double x){
    calls_7++;
    return exp(x);
}

double func4_gsl(double x,void* params){
    
    return exp(x);
}
int main(){
    double a=0; 
    double b=1;
    double err;
    double acc=1e-4;
    double eps=1e-4;
    printf("********************************\nExercise A:Recursive adaptive integrator\n********************************\n\n");
    double Q_1=adapt(square_root,a,b,acc,eps,&err);
    printf("Integral of sqrt(x) from 0 to 1\n");
    printf("integral=%lg \nerror=%lg\n",Q_1, err);
    printf("calls:%d\n\n",calls_1);

    double Q_2=adapt(one_over_sqrt,a,b,acc,eps,&err);
    printf("Integral of 1/sqrt(x) from 0 to 1\n");
    printf("Integral=%lg \nerror=%lg \n",Q_2,err);
    printf("calls:%d\n\n",calls_2);

    double Q_3=adapt(ln_over_sqrt,a,b,acc,eps,&err);
    printf("Integral of ln(x)/sqrt(x) from 0 to 1\n");
    printf("Integral=%lg \nerror=%lg \n",Q_3,err);
    printf("calls:%d\n\n",calls_3);


    double Q_4=adapt(func,a,b,acc,eps,&err);
    printf("Integral of 4*sqrt(1-(1-x)^2) from 0 to 1\n");
    printf("Integral=%lg \nerror=%lg \n",Q_4,err);
    printf("calls:%d\n\n",calls_4);

    printf("*****************************\nExercise B:Clenshaw–Curtis variable transformation\n*****************************\n\n");
    printf("The integrals of 1/sqrt(x) and ln(x)/sqrt(x)\nshow divergencies at the ends of the intervals.\n\n");
    
    calls_2=0;
    double Q_2_CC=clenshaw_curtis(one_over_sqrt,a,b,acc,eps,&err);
    printf("Integral of 1/sqrt(x) from 0 to 1\n");
    printf("integral=%lg \nerror=%lg\n",Q_2_CC, err);
    printf("calls:%d\n\n",calls_2);

    calls_3=0;
    double Q_3_CC=clenshaw_curtis(ln_over_sqrt,a,b,acc,eps,&err);
    printf("Integral of ln(x)/sqrt(x) from 0 to 1\n");
    printf("Integral=%lg \nerror=%lg \n",Q_3_CC,err);
    printf("calls:%d\n\n",calls_3);
    printf("Note:Estimate of integrals closer to the exact values\nNumber of calls significantly smaller.\n");

    printf("\nComparison with gsl routines\nQAGS adaptive integration with singularities\n\n");

    int limit=1000;
	gsl_integration_workspace * w;
	w= gsl_integration_workspace_alloc(limit);
	double result,error;
	gsl_function F;


	F.function= one_over_sqrt_gsl;
    F.params=NULL;
	gsl_integration_qags(&F, 0, 1,acc, eps, limit, w, &result, &error);
    printf("Integral of 1/sqrt(x) from 0 to 1\n");
    printf("integral=%lg \nerror=%lg\nnumber of integrand evaluations:%ld\n\n",result, error,w->size);

    F.function= ln_over_sqrt_gsl;
    F.params=NULL;
	gsl_integration_qags(&F, 0, 1,acc, eps, limit, w, &result, &error);
    printf("Integral of ln(x)/sqrt(x) from 0 to 1\n");
    printf("Integral=%lg \nerror=%lg\nnumber of integrand evaluations:%ld \n\n",result,error,w->size);
	
    printf("*****************************\nExercise C:Infinite limits\n*****************************\n\n");
    //calculate integral x*exp(-x^2) from -infinity to +infinity, result should be 0.
    a=-INFINITY;
    b=+INFINITY;
    double Q_5=adapt(func2,a,b,acc,eps,&err);
    printf("Integral of x*exp(-x^2) from -infinity to +infinity\n");
    printf("Integral=%lg \nerror=%lg \n",Q_5,err);
    printf("calls:%d\n\n",calls_5);

    //calculate integral 1/x^2 from 0 to +infinity, result should be sqrt(pi).
    a=0;
    b=+INFINITY;
    double Q_6=adapt(func3,a,b,acc,eps,&err);
    printf("Integral of 1/(x^2+1) from 0 to +infinity\n");
    printf("Integral=%lg \nerror=%lg \n",Q_6,err);
    printf("calls:%d\n\n",calls_6);

    //calculate integral exp(x) from - infinity to 1, result should be e
    a=-INFINITY;
    b=1;
    double Q_7=adapt(func4,a,b,acc,eps,&err);
    printf("Integral of exp(x) from -infinity to 1 \n");
    printf("Integral=%lg \nerror=%lg \n",Q_7,err);
    printf("calls:%d\n\n",calls_7);

    printf("Compare with gsl methods\nQAGI adaptive integration on infinite intervals\n\n");
    F.function= func2_gsl;
    F.params=NULL;
	gsl_integration_qagi(&F,acc, eps, limit, w, &result, &error);
    printf("Integral of x*exp(-x^2) from -infinity to +infinity\n");
    printf("Integral=%lg \nerror=%lg \nnumber of integrand evaluations:%ld\n\n",result,error,w->size);

    F.function= func3_gsl;
    F.params=NULL;
	gsl_integration_qagiu(&F,0,acc, eps, limit, w, &result, &error);
    printf("Integral of 1/(x^2+1) from 0 to +infinity\n");
    printf("Integral=%lg \nerror=%lg \nnumber of integrand evaluations:%ld\n\n",result,error,w->size);

    F.function= func4_gsl;
    F.params=NULL;
	gsl_integration_qagil(&F,1,acc, eps, limit, w, &result, &error);
    printf("Integral of exp(x) from -infinity to 1\n");
    printf("Integral=%lg \nerror=%lg \nnumber of integrand evaluations:%ld\n\n",result,error,w->size);


    gsl_integration_workspace_free(w);



    return 0;
}