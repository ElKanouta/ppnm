#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>

typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;
ann* ann_alloc(int n, double(*f)(double));
void ann_free(ann* network);
double ann_feed_forward(ann* network, double x);
void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist);
double derivative(ann* network,double x, double df(double));
double antiderivative(ann* network,double start,double stop, double F(double));



double activation_function(double x){
    return x*exp(-x*x);
}

double activation_df(double x){
	return exp(-x*x)+x*(-2*x)*exp(-x*x);
}

double activation_F(double x){
	return -0.5*exp(-x*x);
}

double function_to_fit(double x){
    return cos(x);
}
int main(){
	int n=3; //number of hidden neurons
	ann* network=ann_alloc(n,activation_function);
	double a=0,b=5; //x range
	int nx=10; //number of data points
	gsl_vector* vx=gsl_vector_alloc(nx); //input signal
	gsl_vector* vy=gsl_vector_alloc(nx); //output signal

	for(int i=0;i<nx;i++){
		double x=a+(b-a)*i/(nx-1);
		double f=function_to_fit(x);
		gsl_vector_set(vx,i,x);
		gsl_vector_set(vy,i,f);

	}

	for(int i=0;i<network->n;i++){
		gsl_vector_set(network->data,3*i+0,a+(b-a)*i/(network->n-1));
		gsl_vector_set(network->data,3*i+1,1);
		gsl_vector_set(network->data,3*i+2,1);
	}
	
	ann_train(network,vx,vy);

	for(int i=0;i<vx->size;i++){
		double x=gsl_vector_get(vx,i);
		double f=gsl_vector_get(vy,i);
		printf("%g %g\n",x,f);
	}
	printf("\n\n");

	double dz=1.0/64;
	for(double z=a;z<=b;z+=dz){
		double y=ann_feed_forward(network,z);
		double der=derivative(network,z,activation_df);
		double anti_der=antiderivative(network,a,z,activation_F);
		printf("%g %g %g %g\n",z,y, der, anti_der);
	}



gsl_vector_free(vx);
gsl_vector_free(vy);
ann_free(network);
return 0;
}