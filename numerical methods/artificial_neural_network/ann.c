#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

int qnewton(double f(gsl_vector*), gsl_vector*x, double epsilon);
//int qnewton(double beta(gsl_vector*), gsl_vector*x, double acc) ;

typedef struct {int n; double (*f)(double); gsl_vector* data;} ann;

ann* ann_alloc(int n, double(*f)(double)){
    ann* network = malloc(sizeof(ann));
	network->n=n;
	network->f=f;
	network->data=gsl_vector_alloc(3*n); //{a1,...,an,b1,...bn,w1,..,wn}
	return network;
}

void ann_free(ann* network){
    gsl_vector_free(network->data);
    free(network);
}

double ann_feed_forward(ann* network, double x){
    double s=0; //result of summation neuron
	for(int i=0;i<network->n;i++){
		double a=gsl_vector_get(network->data,3*i+0);
		double b=gsl_vector_get(network->data,3*i+1);
		double w=gsl_vector_get(network->data,3*i+2);
		s+=network->f((x-a)/b)*w;
	}
	return s;

}

void ann_train(ann* network, gsl_vector* vx, gsl_vector* vy){ //tuning its parameters to minimize the deviation 
    
	double delta(gsl_vector* p){
		gsl_vector_memcpy(network->data,p);
		
		double s=0;
		for(int i=0;i<vx->size;i++){
			double x=gsl_vector_get(vx,i);
			double f=gsl_vector_get(vy,i);
			double y=ann_feed_forward(network,x);
			s+=fabs(y-f);
		}
		return s/vx->size;
	}

	gsl_vector* p=gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p,network->data);
	qnewton(delta, p, 1e-3);


	gsl_vector_memcpy(network->data,p);
	gsl_vector_free(p);

}

// derivative and antiderivative functions are variations of feed forward function
double derivative(ann* network,double x, double df(double)){
	double s=0; //result of summation neuron
	for(int i=0;i<network->n;i++){
		double a=gsl_vector_get(network->data,3*i+0);
		double b=gsl_vector_get(network->data,3*i+1);
		double w=gsl_vector_get(network->data,3*i+2);
		s+=df((x-a)/b)*w/b;
	}
	return s;
}

double antiderivative(ann* network,double start,double stop, double F(double)){
	double s=0; //result of summation neuron
	for(int i=0;i<network->n;i++){
		double a=gsl_vector_get(network->data,3*i+0);
		double b=gsl_vector_get(network->data,3*i+1);
		double w=gsl_vector_get(network->data,3*i+2);
		s+=w*b*(F((a-stop)/b)-F((a-start)/b));
	}
	return s;
}

