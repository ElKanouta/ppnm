#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int main(int argc, char** argv){
	printf("\n\nTable: x - sin(x)\n");
	for(int i=1;i<argc;i++) {
		double x=atof(argv[i]);
		printf("\n%lg \t %lg\n",x,sin(x));
	}
return 0;
}
