#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>

double rosenbrock_f (const gsl_vector * x, void *params, gsl_vector * f){
    const double x0 = gsl_vector_get (x, 0);
    const double x1 = gsl_vector_get (x, 1);
    const double f1=x1-x0*x0;
    const double f2=1-x0;
    return 100*f1*f1 + f2*f2;

}

int main(int argc, char** argv){
    int dim=2; //dimension
    //starting point
    gsl_vector *x, *ss;
    x=gsl_vector_alloc(2);
    gsl_vector_set(x,0,-1);
    gsl_vector_set(x,1,-1);

    //initial step size
    ss= gsl_vector_alloc(2);
    gsl_vector_set_all(ss, 0.1);

    gsl_multimin_fminimizer *s;
    const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
    s = gsl_multimin_fminimizer_alloc(T, dim);//type, dimension
    gsl_multimin_function F;
    F.n=dim;
    F.f=rosenbrock_f;
    gsl_multimin_fminimizer_set(s,&F,x,ss);

    int iter=0; //initialize number of iterations
    int status;
    do{
        iter++;
        status=gsl_multimin_fminimizer_iterate(s);
        if(status){
            break;
        }
        double size=gsl_multimin_fminimizer_size(s);
        status=gsl_multimin_test_size(size, 1e-4);
        if(status=GSL_SUCCESS){
            printf("converged to minimunm at\n");
        }
        printf("Iteration:%i\t x=%g\t y=%g\t f(x,y)=%g\t size=%g\n",iter, gsl_vector_get(s-> x,0),gsl_vector_get(s->x,1),s->f, size);
//to do:check while condition, does not iterate
    }while (status=GSL_CONTINUE && iter<100);
    gsl_vector_free(x);
    gsl_vector_free(ss);

    gsl_multimin_fminimizer_free(s);
    return status;
}

