//3D minimization problem

#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<stdlib.h>

struct experimental_data {int n; double *t,*y,*e;};

//function to minimize
double function_to_minimize (const gsl_vector *x, void *params) {
	//fitting parameters
	double  A = gsl_vector_get(x,0);
	double  T = gsl_vector_get(x,1);
	double  B = gsl_vector_get(x,2);
	struct experimental_data *p = (struct experimental_data*) params;
	int     n = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum=0;
	#define f(t) A*exp(-(t)/T) + B
	for(int i=0;i<n;i++){
         sum+= pow( (f(t[i]) - y[i])/e[i] ,2);
    }
    return sum;
}

int main(int argc, char** argv){
    if(argc<2){
		fprintf(stderr, "usage:%s number_of_lines+to_read \n",argv[0]);
	}
	int n=atoi(argv[1]);
	double Tim[n],Y[n],error[n];//time, activity, error
	for(int i=0;i<n;i++){
		scanf("%lg %lg %lg",Tim+i, Y+i, error+i);
	}
	for(int i=0;i<n;i++){
                printf("%g %g %g\n",*(Tim+i), *(Y+i), *(error+i));
        }

    int dim=3; //dimension
    struct experimental_data data;
    data.n=n;
    data.t=Tim;
    data.y=Y;
    data.e=error;

    //function
    gsl_multimin_function F;
    F.f= function_to_minimize;
    F.n=dim;
    F.params=(void*)&data;

    gsl_multimin_fminimizer *M;
    #define TYPE gsl_multimin_fminimizer_nmsimplex2
	M=gsl_multimin_fminimizer_alloc(TYPE,dim);
	gsl_vector* start=gsl_vector_alloc(dim);
	gsl_vector* step=gsl_vector_alloc(dim);
	//initialize
    gsl_vector_set(start,0,0.4);
	gsl_vector_set(start,1,5.4);
	gsl_vector_set(start,2,0.2);
    //step
	gsl_vector_set(step,0,0.5);
	gsl_vector_set(step,1,0.2);
	gsl_vector_set(step,2,0.05);

    gsl_multimin_fminimizer_set(M,&F,start,step);

	int iter=0,status, iter_status;
	double size;
	do{
		iter++;
		iter_status = gsl_multimin_fminimizer_iterate(M);
		if (iter_status!=0) break;

		size = gsl_multimin_fminimizer_size (M);
		status = gsl_multimin_test_size (size, 1e-2);

		if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }

        printf ("iter=%5d A=%g T=%g B=%g size=%g fval=%g\n",
              iter,
              gsl_vector_get (M->x, 0),
              gsl_vector_get (M->x, 1),
              gsl_vector_get (M->x, 2),
               size, M->fval);
    }
  while (status == GSL_CONTINUE && iter < 100);

	double A=gsl_vector_get(M->x,0);
	double T=gsl_vector_get(M->x,1);
	double B=gsl_vector_get(M->x,2);
	//check main.c on multimin-decay examples
	//double dt=()
	for(int i=0;i<n;i++)
		fprintf(stderr,"%g %g %g %g\n",
		*(Tim+i),*(Y+i),*(error+i)
		,f(Tim[i])
		);
	printf("found A=%g, T=%g, B=%g\n",A,T,B);
return 0;
}
