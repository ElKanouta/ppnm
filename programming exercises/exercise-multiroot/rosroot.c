#include<stdio.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>

//rosenbrock function gradient
int grad(const gsl_vector * x, void * params, gsl_vector * f){
    const double x1=gsl_vector_get(x,0);
    const double x2=gsl_vector_get(x,1);
    double gr1=400*x1*x1*x1 -400*x1*x2 +2*x1 -2;
    double gr2=200*x2 -200*x1*x1;
    gsl_vector_set(f,0,gr1);
    gsl_vector_set(f,1,gr2);
    return GSL_SUCCESS;
}

int find_root(double *y1, double *y2){
    gsl_multiroot_function F;
    F.f=grad;
    F.n=2;
    F.params=NULL;
    gsl_multiroot_fsolver * S;
    S=gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,F.n);
    gsl_vector * start=gsl_vector_alloc(F.n);
    gsl_vector_set(start,0,*y1);
    gsl_vector_set(start,1,*y2);
    gsl_multiroot_fsolver_set(S,&F,start);
    
    int flag;
    int iter=0;
    double eps=1e-12;
    do{
        iter++;
        gsl_multiroot_fsolver_iterate(S);
        flag=gsl_multiroot_test_residual(S->f,eps);
        fprintf(stdout,"iteration:%i\tPoint:x=%g, y=%g\tGradient:Grx=%g, Gry=%g\n",iter,gsl_vector_get(S->x,0),gsl_vector_get(S->x,1),gsl_vector_get(S->f,0),gsl_vector_get(S->f,1));


    }
    while(flag==GSL_CONTINUE);

    //points of minimum
    *y1=gsl_vector_get(S->x,0);
    *y2=gsl_vector_get(S->x,1);
    gsl_vector_free(start);
    gsl_multiroot_fsolver_free(S);
    return 0;

}
int main(){
    double *rootx, *rooty, x, y;
    //initialize
    x=0.5;
    y=0.5;
    rootx=&x;
    rooty=&y;
    printf("Exercise 1: Gradient of rosenbrock function\n");
    find_root(rootx,rooty);
    return 0;
}