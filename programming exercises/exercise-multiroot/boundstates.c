#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<math.h>

//solve the radial differential equation
int radial(double r, const double f[], double dfdr[], void *params){
    double e=*(double*)params;
    dfdr[0]=f[1];
    dfdr[1]=-2*(e*f[0]+(1/r)*f[0]);
    return GSL_SUCCESS;
}

double radial_solution(double rmax,double e){
	gsl_odeiv2_system sys;
	sys.function = radial;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&e;

	gsl_odeiv2_driver *driver;
	double hstart = 0.001, abs = 1e-8, eps = 1e-8;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys,
					       gsl_odeiv2_step_rkf45,
					       hstart, abs, eps);

	double x0 = 1e-4;
	double f[] = { x0-x0*x0, 1-2*x0};
	gsl_odeiv2_driver_apply(driver, &x0, rmax, f);
	gsl_odeiv2_driver_free(driver);
	return f[0];

}
//the function we use in root finding
int H_equation(const gsl_vector * x, void * params, gsl_vector * f){
    double rmax=*(double*)params;
    double e=gsl_vector_get(x,0);
    double solution= radial_solution(rmax,e);
    gsl_vector_set(f,0,solution);
    return GSL_SUCCESS;
}


//find  lowest root of M(e)=0
int main(){
    //define max radious and energy e
    double rmax=8;
    double estart=-0.5; //try
    
  /*  //check the result from 1st function
    double g=radial_solution(rmax,-0.5);
    printf("f(rmax)=%g\n",g); */

    
    
    gsl_multiroot_function F;
    F.f=H_equation;
    F.n=1;
    F.params=(void*)&rmax;
    

    gsl_multiroot_fsolver *Solv;
    Solv= gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids, F.n);
    gsl_vector *start=gsl_vector_alloc(F.n);
    gsl_vector_set(start, 0,estart);
    gsl_multiroot_fsolver_set(Solv, &F, start);

    int flag;
    double EPS=1e-6;
    do{
        gsl_multiroot_fsolver_iterate(Solv);
        
        flag=gsl_multiroot_test_residual(Solv->f,EPS);
        
    }while(flag==GSL_CONTINUE && Solv<0);

    double energy_result=gsl_vector_get(Solv->x,0); 
    
    gsl_multiroot_fsolver_free(Solv);
    gsl_vector_free(start);
    printf("Eercise 2:\n");
    printf("Energy root found:%g\n",energy_result);
    printf("Expected energy:%g\n",estart);

    //plot the radial solution and compare with the exact result f0(r)=r*exp(-r)
    for(int i=1; i<100; i++){
        fprintf(stderr,"%g %g %g\n",rmax*i/100, radial_solution(rmax*i/100,energy_result),(rmax*i/100)*exp(-rmax*i/100));
    }
return 0;

}
