#include<stdio.h>
#include<limits.h>
#include<float.h>

int main(){
	int i=1;
	printf("min representable integer = %i\n", INT_MIN);
	printf("\nusing while loop:\n");
	while(i-1<i){
		i--;
		}
	printf("min integer = %i \n", i);
	printf("\nusing for loop:\n");
	for(i=0;i-1<i;i--){
	}
	printf("min integer: %i\n",i);
	printf("\nusing do while:\n");
	do{
		i--;
	}
	while(i-1<i);
	printf("min integer: %i\n",i);
	printf("**********\n");
return 0;
}


