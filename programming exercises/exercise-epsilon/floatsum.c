#include<stdio.h>
#include<limits.h>

int main(){
	int max=INT_MAX/2;
	int i;
	float sum_up=0;
	float sum_down=0;
	for(i=1;i<max;i++){
		sum_up=sum_up +1.0f/i;
		}
	for(i=max;i>=1;i--){
		sum_down=sum_down+1.0f/i;
		}
	printf("Sum of float numbers:\n");
	printf("Sum up float= %g\nSum down float= %g\n", sum_up, sum_down);
	printf("Downwards sum does not neglect some small terms, because the add up in the beginning, before the large numbers.\n");
	printf("\nSum of double numbers:\n");
	double sum_up_d=0;
	double sum_down_d=0;
	double b=1;
	for(i=1;i<max;i++){
		sum_up_d=sum_up_d +(double)1.0/i;
		}
	for(i=max;i>=1;i--){
		sum_down_d=sum_down_d+(double)1.0/i;
		}
	printf("Sum up double=%g\nSum down double=%g\n",sum_up_d,sum_down_d);
	printf("Double numbers have better accuracy\n and we have to show more digits \n to see the difference between the two sums.\n");
	printf("**********\n");
return 0;
}

