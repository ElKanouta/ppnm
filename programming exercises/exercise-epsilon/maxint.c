#include<stdio.h>
#include<limits.h>
#include<float.h>

int main(){
	int i=1;
	printf("max representable integer = %i\n", INT_MAX);
	printf("\nusing while loop:\n");
	while(i+1>i){
		i++;
		}
	printf("max integer = %i \n", i);
	
	printf("\nusing for loop:\n");
	for(i=0;i<i+1;i++){
		i+=i;
	}
	printf("max integer: %i\n",i);
	printf("\nusing do while:\n");
	do{
		i++;
	}
	while(i<i+1);
	printf("max integer: %i\n",i);
	printf("**********\n");

return 0;
}


