#include<stdio.h>
#include<float.h>

int main(){
	float x1=1;
	double y1=1;
	long double z1=1;
	printf("values from float.h:\n FLT_EPSILON= %g\n DBL_EPSILON= %g\n LDBL_EPSILON= %Lg\n",FLT_EPSILON, DBL_EPSILON, LDBL_EPSILON);
	printf("\nusing while loop:\n");
	while(1+x1!=1){x1/=2;} x1*=2;
	printf("machine epsilon for float: %g\n",x1);
	while(1+y1!=1){y1/=2;} y1*=2;
	printf("machine epsilon for double: %g\n ",y1);
	while(1+z1!=1){z1/=2;} z1*=2;
	printf("machine epsilon for long double: %Lg\n ",z1);
	float x2=1;
	double y2=1;
	long double z2=1;
	printf("\nusing for loop:\n");
	for(x2=1;1+x2!=1;x2/=2){}
	x2*=2;
	printf("machine epsilon for float: %g\n",x2);
	for(y2=1;1+y2!=1;y2/=2){}
	y2*=2;
	printf("machine epsilon for double: %g\n ",y2);
	for(z2=1;1+z2!=1;z2/=2){}
	z2*=2;
	printf("machine epsilon for long double: %Lg\n ",z2);
	printf("\nusing do while:\n");
	float x3=1;
	double y3=1;
	long double z3=1;
	do{
		x3/=2;
	}
	while(1+x3!=1);
	x3*=2;
	printf("machine epsilon for float: %g\n",x3);
	do{
		y3/=2;
	}
	while(1+y3!=1);
	y3*=2;
	printf("machine epsilon for double: %g\n ",y3);
	do{
		z3/=2;
	}
	while(1+z3!=1);
	z3*=2;
	printf("machine epsilon for long double: %Lg\n ",z3);
	printf("**********\n");
return 0;
}

