#include<stdio.h>
#include<math.h>
#include<stdlib.h>

double a;
double b;
double tau;
double epsilon;
int equal(double a, double b, double tau, double epsilon){
	double abs_precis=fabs(a-b);
	int r;
	if(abs_precis < tau){
		r=1;
		}
	else if(abs_precis/(fabs(a)+fabs(b))<epsilon/2){
		r=1;}

	else{
		r=0;
		}
return r;
}

int main(){
	double a,b,tau, epsilon;
	// double b=atof(argv[2]);
	// double tau=atof(argv[3]);
	// double epsilon=atof(argv[4]);
	printf("Equal function\nGive values for a b tau epsilon:");
	scanf("%lg %lg %lg %lg",&a,&b,&tau,&epsilon);
	printf("a=%lg, b=%lg, tau=%lg, epsilon=%lg\n",a,b,tau,epsilon);
	printf("fabs:%g\n",fabs(a-b));
	int result=equal(a,b,tau, epsilon);
	printf("return: %i\n",result);
	
return 0;
}

