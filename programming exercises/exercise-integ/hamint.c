#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double hamint(double y, void* params){
	double a=*(double *) params;
	double h=(-(a*y*y)/2 +a/2 + (y*y)/2)*exp(-a*y*y);
	return h;
}

double hamiltonian(double a) {
	int limit=10000;
	gsl_integration_workspace * w;
	w= gsl_integration_workspace_alloc(limit);
	double result, error,acc=1e-8, eps=1e-8;

	gsl_function H;
	H.function=&hamint;
	H.params=(void*)&a;

	int Hamiltonian_integral=gsl_integration_qagi(&H,eps,acc,limit,w ,&result, &error);

	gsl_integration_workspace_free(w);
	if(Hamiltonian_integral!=GSL_SUCCESS) return NAN;
	else return result;

}
