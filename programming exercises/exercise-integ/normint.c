#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double normint(double x, void* params){
	double a=*(double *) params;
	double f=exp(-a*(x*x));
	return f;
}


double norm(double a){
	int limit=10000;
	gsl_integration_workspace * w;
	w= gsl_integration_workspace_alloc(limit);

	gsl_function N;
	N.function=&normint;
	N.params=(void*)&a;

	double result, error, acc=1e-8, eps=1e-8;
	int Norm_integral=gsl_integration_qagi(&N,eps,acc, limit, w, &result, &error);


	gsl_integration_workspace_free(w);
	//printf("Norm integral:%g\n",result);
	if(Norm_integral!=GSL_SUCCESS) return NAN;
	else return result;


}
