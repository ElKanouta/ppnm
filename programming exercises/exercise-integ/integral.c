#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>

double integ( double x, void* params){
	
	double f=log(x)/sqrt(x);
	return f;
}

int main(){
	int limit=1000;
	gsl_integration_workspace * w;
	w= gsl_integration_workspace_alloc(limit);
	double result,error, acc=1e-8, eps=1e-8;
	gsl_function F;
	F.function= integ;

	gsl_integration_qags(&F, 0, 1,acc, eps, limit, w, &result, &error);

	gsl_integration_workspace_free(w);

	printf("Exercise 1\nresult:%g \n",result);
return 0;
}
