//linear equations in matrix form
//solve with gsl_linalg_LU_decomp / _solve
#include<stdio.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
int main(){
	double A[]={6.13, -2.9, 5.86,8.08,-6.31,-3.89,-4.36,1.00,0.19};
	double bv[]={6.23,5.37,2.29};
	gsl_matrix_view m=gsl_matrix_view_array (A, 3,3);
	gsl_vector_view b=gsl_vector_view_array (bv, 3);
	gsl_vector *x=gsl_vector_alloc(3);
	int s;
	gsl_permutation *p=gsl_permutation_alloc(3);
	gsl_linalg_LU_decomp(&m.matrix, p, &s);
	gsl_linalg_LU_solve(&m.matrix, p, &b.vector, x);
	printf("GSL-LU-decomposition\nResult\nx= \n");
	gsl_vector_fprintf(stdout,x,"%g");
	gsl_permutation_free (p);
	gsl_vector_free(x);


	
	//check the solution
	//multiply the original matrix (set here as B) with the solution (sol)
	gsl_vector *y=gsl_vector_alloc(3);
	gsl_matrix *B=gsl_matrix_alloc(3,3);
	
	//matrix_set
	gsl_matrix_set(B,0,0,6.13);
	gsl_matrix_set(B,0,1,-2.9);
	gsl_matrix_set(B,0,2,5.86);
	gsl_matrix_set(B,1,0,8.08);
	gsl_matrix_set(B,1,1,-6.31);
	gsl_matrix_set(B,1,2,-3.89);
	gsl_matrix_set(B,2,0,-4.36);
	gsl_matrix_set(B,2,1,1);
	gsl_matrix_set(B,2,2,0.19);

	//vector_set for the result
	gsl_vector_set(y,0,0);
	gsl_vector_set(y,1,0);
	gsl_vector_set(y,2,0);

	gsl_vector *sol=gsl_vector_alloc(3);
	//vector_set for the solution
	gsl_vector_set(sol,0,-1.1379);
	gsl_vector_set(sol,1,-2.83303);
	gsl_vector_set(sol,2,0.851459);
	
	gsl_blas_dgemv(CblasNoTrans,1 , B, sol, 0, y);
	printf("check the result by multiplying it with the matrix\nIt gives:\n");
	
	gsl_vector_fprintf(stdout,y,"%g"); 
	gsl_vector_free(y);
	gsl_matrix_free(B);
	printf("...\nThe result of the multiplication should give:\n");
	gsl_vector_fprintf(stdout, &b, "%g");
return 0;
}
