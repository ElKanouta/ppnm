#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_odeiv2.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_sf.h>

int ode(double x, const double u[],double dudx[], void* params){
    dudx[0]=(2/sqrt(M_PI))*exp(-x*x);
    return GSL_SUCCESS;
}

double error(double x){
    //odeiv2 system
    gsl_odeiv2_system sys;
    sys.function=ode;
    sys.jacobian=NULL;
    sys.dimension=1;
    sys.params=NULL;

    
    gsl_odeiv2_driver* driver;
    double hstart=1e-4, epsabs=1e-8, epsrel=1e-8 ;
    driver=gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45,hstart,epsabs,epsrel);
    //initial conditions
    double x0=0;
    double u[1]={0.0};
    
    gsl_odeiv2_driver_apply(driver, &x0,x,u);
    gsl_odeiv2_driver_free(driver);
    return u[0];

}

int main(int argc, char** argv){
    double a=atof(argv[1]);
    double b=atof(argv[2]);
    double dx=atof(argv[3]);

    double x,y;
    for(x=a;x<=b; x+=dx){
        if(x<0){
            y=-error(-x);
        }
        else{
            y=error(x);
        }
        fprintf(stdout,"%g %g %g\n",x, y, gsl_sf_erf(x));
    }
    return 0;
}

