#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

struct harm {int a,b; double result;};

/* 
The center of the square is at (0.5,0.5)
The radius of the circle is 0.5
*/
void* hit(void* param){
    int count=0 , darts=0; //number of hits and throws
    struct harm* data = (struct harm*)param;
    int a=data->a;
    int b=data->b;
    unsigned int *seed;
    
    
    //position of dart
    for(int i=a;i<b;i++){
        darts++;
        double x=rand_r(&(seed))/(double)RAND_MAX-0.5;
        double y=rand_r(&(seed))/(double)RAND_MAX-0.5;
        double pos=sqrt(x*x +y*y);
        
        //check if dart inside the circle
        if(pos<=0.5){
            count++;
        }
    }
    double pi= 4*(double)count/(double)darts;
    data->result=pi;
      
}


int main(){
    int n=1e6; //number of throws
    int mid=n/2; //number of throws in each thread
    struct harm data1, data2, data3, data4, data5;
    //starting and ending points
    data1.a=1;
    data1.b=mid;
    data2.a=mid;
    data2.b=n;
    pthread_t thread1,thread2, thread3; //2 threads to calculeate pi at the same time
    
    //thread1
    pthread_create(&thread1, NULL, hit,(void*)&data1 );
    
    
    //thread2
    pthread_create(&thread2, NULL, hit,(void*)&data2 );
    pthread_join(thread1,NULL);

    pthread_join(thread2,NULL);
    
    double pi=(data1.result+data2.result)/2;//mean value of pi
    printf("Pi calculated with pthreads:\nPi is: %g\n",pi);

    //******FIND PI WITH OPENMP**********//
    
    data3.a=1;
    data3.b=mid;
    data4.a=mid;
    data4.b=n;
    
    #pragma omp parallel sections
    {
    #pragma omp section
        {
        hit((void*)&data3);
        }
    #pragma omp section
        {
        hit((void*)&data4);
        }
    }
    double pi_2=(data3.result+data4.result)/2;

     
    printf("Pi calculated with openMp:\nPi is:%g\n",pi_2);
    
    //********Convergence**********//
    
    data5.a=1;
    data5.b;
    
    for(data5.b=100;data5.b<1e7;data5.b=data5.b*2){
        pthread_create(&thread3, NULL, hit,(void*)&data5 );
        pthread_join(thread1,NULL);
        
        fprintf(stderr,"%i %g %g\n",data5.b,data5.result,M_PI-data5.result);
    }
    
    return EXIT_SUCCESS;
} 



