//exercise 27
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_eigen.h>

double matrix(int n){
    
    gsl_matrix *m=gsl_matrix_alloc(n,n);
    
    for(int i=0;i<=n-1;i++){
        for(int j=0;j<=n-1;j++){
            gsl_matrix_set(m,i,j,1);
            
        }
    }
    gsl_eigen_symm_workspace *W;
    W=gsl_eigen_symm_alloc(n);
    gsl_vector *eval=gsl_vector_alloc(n);
    gsl_eigen_symm(m,eval,W);
    gsl_sort_vector(eval);

    double max_eigen=gsl_vector_get(eval,n-1);
    gsl_eigen_symm_free(W);
    gsl_vector_free(eval);
    gsl_matrix_free(m);
    return max_eigen;
}

int main(){
    
    for(int n=1;n<=20;n++){
        printf("%i %g\n",n,matrix(n));
    }
    return 0;
}