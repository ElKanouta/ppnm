#include<stdio.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
//#define M_PI 3.141592653

int orbital_equation
(double phi, const double y[], double yprime[], void *params)
{
double epsilon = *(double *) params;
yprime[0] = y[1];
yprime[1] = 1 - y[0] + epsilon * y[0] * y[0];
return GSL_SUCCESS;
}

double orbit(double t, double epsilon, double uprime){


	//system: function, jacobian, dimension, params
	gsl_odeiv2_system orb={orbital_equation, NULL, 2,(void *)&epsilon};
	
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	

	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(&orb, gsl_odeiv2_step_rkf45, hstart, epsabs, epsrel);

	//initial values
	double t0 = 0, y[2] = { 1, uprime };

	
	gsl_odeiv2_driver_apply (driver, &t0, t, y);
	

	gsl_odeiv2_driver_free (driver);
return y[0];
}
int main(){
	//case 1
	double epsilon1=0;
	double uprime1=0;
	//case 2
	double epsilon2=0;
	double uprime2=-0.5;
	//case 3
	double epsilon3=0.01;
	double uprime3=-0.5;
	
	double phi_max = 39.5 * M_PI, delta_phi = 0.05;
	for (double phi = 0; phi < phi_max; phi += delta_phi){
		fprintf(stderr,"%g %g %g %g\n",phi,orbit(phi, epsilon1,uprime1),orbit(phi, epsilon2,uprime2),orbit(phi, epsilon3,uprime3));
	}
return 0;
}
 