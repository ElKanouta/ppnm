#include<gsl/gsl_odeiv2.h>
#include<stdio.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int integrate(double x,const double y[], double dydt[], void *params){
    dydt[0]=y[0]*(1-y[0]);
    return GSL_SUCCESS;
}


double func(double x){
    int dim=1; //dimension
    
    //system:function, jacobian, dimension, params
    gsl_odeiv2_system integ={integrate, NULL, dim, NULL};
    //integ.function=integrate;
    //integ.jacobian= NULL;
    //integ.dimension= dim;
    //integ.params= NULL;
    
    double epsabs=1e-5, epsrel=1e-5;
    double hstart=0.1;
    gsl_odeiv2_driver* driver= gsl_odeiv2_driver_alloc_y_new(&integ, gsl_odeiv2_step_rkf45,hstart, epsabs, epsrel);
    double y[]={0.5 }; //initial value
    double start=0;//starting point
    gsl_odeiv2_driver_apply(driver,&start,x,y);
    
    gsl_odeiv2_driver_free(driver);
return y[0];
}

//compare with logistic function
//standard logistic function f(x)=1/(1+exp(-x))
int main(){
    double z;
    for(z=(0);z<3;z+=0.1){
        fprintf(stderr,"%g %g %g\n",z,func(z),1/(1+exp(-z)));
    }
return 0;
}