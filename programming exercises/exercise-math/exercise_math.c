#include <stdio.h>
#include <complex.h>
#include <math.h>

int main(){
	//calculate gamma function
	double g=tgamma(5);
	//calculate bessel function
	double ji=j1(0.5);
	//calculate square root
	double sqrt_real=creal(csqrt(-2));
	double sqrt_im=cimag(csqrt(-2));
	//calculate exponentials //different variables for real and imaginary values
	double e_i_real=creal(cexp(I));
	double e_i_im=cimag(cexp(I));
	double e_ipi_real=creal(exp(I*M_PI));
	double e_ipi_imag=cimag(exp(I*M_PI));
	double i_e_real=creal(cpow(I,M_E));
	double i_e_im=cimag(cpow(I,M_E));
	printf(" gamma function: %f \n bessel function: %f \n square root: %f + (%f) i \n exponential(i): %f + (%f) i \n exponential(i*pi): %f + (%f) i \n i^e: %f + (%f) i\n" , g,ji, sqrt_real, sqrt_im, e_i_real, e_i_im, e_ipi_real, e_ipi_imag, i_e_real, i_e_im);
	printf(" 1.0f/9 float : %.25g\n 1.0 /9 double: %.25lg\n 1.0L/9 long double : %.25Lg\n", 1.0f /9, 1.0 /9, 1.0L/9);

	return 0;

}


